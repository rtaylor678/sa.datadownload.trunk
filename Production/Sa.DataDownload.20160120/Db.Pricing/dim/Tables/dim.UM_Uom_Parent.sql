﻿CREATE TABLE [dim].[UM_Uom_Parent]
(
	[UM_MethodologyId]		INT					NOT	NULL,
	[UM_UomId]				INT					NOT NULL	CONSTRAINT [FK__UM_Uom_Parent_UM_UomId]				REFERENCES [dim].[UM_Uom_LookUp]([UM_UomId]),
	[UM_ParentId]			INT					NOT NULL	CONSTRAINT [FK__UM_Uom_Parent_UM_Parent]			REFERENCES [dim].[UM_Uom_LookUp]([UM_UomId])
															CONSTRAINT [FK__UM_Uom_Parent_UM_ParentParent]
															FOREIGN KEY ([UM_MethodologyId], [UM_UomId])
															REFERENCES [dim].[UM_Uom_Parent]([UM_MethodologyId], [UM_UomId]),

	[UM_Operator]			CHAR(1)				NOT	NULL	CONSTRAINT [DF__UM_Uom_Parent_UM_Operator]			DEFAULT('+')
															CONSTRAINT [FK__UM_Uom_Parent_UM_Operator]			REFERENCES [dim].[OP_Operator_LookUp]([OP_OperatorId]),
	[UM_SortKey]			INT					NOT	NULL,
	[UM_Hierarchy]			SYS.HIERARCHYID		NOT	NULL,

	[UM_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__UM_Uom_Parent_UM_PositedBy]			DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__UM_Uom_Parent_UM_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[UM_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__UM_Uom_Parent_UM_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[UM_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__UM_Uom_Parent_UM_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__UM_Uom_Parent_UM_RowGuid]			UNIQUE NONCLUSTERED([UM_RowGuid]),

	CONSTRAINT [PK__UM_Uom_Parent]			PRIMARY KEY CLUSTERED ([UM_MethodologyId] DESC, [UM_UomId] ASC)
);