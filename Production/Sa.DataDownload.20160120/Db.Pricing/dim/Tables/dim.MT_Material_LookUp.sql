﻿CREATE TABLE [dim].[MT_Material_LookUp]
(
	[MT_MaterialId]			INT					NOT NULL	IDENTITY(1, 1),

	[MT_Tag]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__MT_Material_LookUp_MT_Tag]				CHECK([MT_Tag] <> ''),
															CONSTRAINT [UK__MT_Material_LookUp_MT_Tag]				UNIQUE CLUSTERED([MT_Tag] ASC),

	[MT_Name]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__MT_Material_LookUp_MT_Name]				CHECK([MT_Name] <> '')
															CONSTRAINT [UX__MT_Material_LookUp_MT_Name]				UNIQUE([MT_Name] ASC),

	[MT_Detail]				NVARCHAR(192)		NOT	NULL	CONSTRAINT [CL__MT_Material_LookUp_MT_Detail]			CHECK([MT_Detail] <> '')
															CONSTRAINT [UX__MT_Material_LookUp_MT_Detail]			UNIQUE([MT_Detail] ASC),

	[MT_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__MT_Material_LookUp_MT_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__MT_Material_LookUp_MT_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[MT_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__MT_Material_LookUp_MT_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[MT_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__MT_Material_LookUp_MT_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__MT_Material_LookUp_MT_RowGuid]			UNIQUE NONCLUSTERED([MT_RowGuid]),

	CONSTRAINT [PK__MT_Material_LookUp]			PRIMARY KEY([MT_MaterialId] ASC)
);