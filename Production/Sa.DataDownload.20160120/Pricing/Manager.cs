﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pricing;
using Solomon.Common.FileOps;

using System.IO;

namespace Pricing
{
    public class Manager//:IDisposable
    {
        //private Manager _manager = new Manager();
        //private bool isDisposed = false;

        public Manager()
        {
        }


        public List<String> GetDirectoriesFromFtpSite(ref string error, string ftpSite, string ftpUser, string ftpPassword, string ftpDirectory)
        {

            //if (isDisposed) return null;

            List<string> directoryList = new List<string>();

            try
            {
                //Create ftp object
                Ftp ftp = new Ftp(ftpSite, ftpUser, ftpPassword);
                string[] directories = ftp.DirectoryListSimple(ref error, ftpDirectory);

                foreach (string item in directories)
                {
                    directoryList.Add(item);
                }


                return directoryList;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }

        public List<String> GetDirectoriesFromFtpSiteWithDetails(ref string error, string ftpSite, string ftpUser, string ftpPassword, string ftpDirectory, string localUploadDir)
        {
            //if (isDisposed) return null;

            List<string> directoryList = new List<string>();

            try
            {
                //Create ftp object
                Ftp ftp = new Ftp(ftpSite, ftpUser, ftpPassword);
                string[] directories = ftp.DirectoryListDetailed(ref error, ftpDirectory);

                foreach (string item in directories)
                {
                    directoryList.Add(item);
                }

                return directoryList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<String> GetFilesFromFtpSiteDirectory(ref string error, string currFtpDirectory, string ftpSite, string ftpUser, string ftpPassword, string processKey)
        {

            //if (isDisposed) return null;

            List<string> fileList = new List<string>();

            try
            {
                ////Create ftp object
                Ftp ftp = new Ftp(ftpSite, ftpUser, ftpPassword);
                string[] files = ftp.DirectoryFiles(ref error, currFtpDirectory);

                foreach (string item in files)
                {
                    //string fileInfo = item.Split(';

                    if (processKey == "Platts")
                    {
                        if (item.EndsWith(".ftp"))
                        {
                            fileList.Add(item);
                        }
                    }

                    if (processKey == "Argus")
                    {
                        if (item.EndsWith(".csv"))
                        {
                            fileList.Add(item);
                        }
                    }
                }

                return fileList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GetMostRecentDirectory(string path)
        {
            //if (isDisposed) return null;

            string mostRecentDir = string.Empty;
            string currDir = string.Empty;

            FileHandler fileHandler = new FileHandler();

            try
            {

                if (!fileHandler.DirectoryExists(path))
                {
                    return string.Empty;
                }
                else
                {
                    string[] dirs = Directory.GetDirectories(path);


                    int rowIndex = 0;

                    foreach (var dir in dirs)
                    {
                        if (rowIndex == 0)
                        {
                            mostRecentDir = GetDirectoryName(dir.ToString());

                        }
                        else
                        {
                            currDir = GetDirectoryName(dir.ToString());

                            if (Convert.ToInt32(currDir) > Convert.ToInt32(mostRecentDir))
                            {
                                mostRecentDir = currDir;
                            }
                        }

                        rowIndex = rowIndex + 1;
                    }

                    return mostRecentDir;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<String> GetDirectories(ref string error, string path)
        {
            //if (isDisposed) return null;

            List<string> directoryList = new List<string>();
            FileHandler fh = new FileHandler();
            string[] directories;


            try
            {
                if (fh.DirectoryExists(path))
                {
                    directories = Directory.GetDirectories(path);

                    foreach (string item in directories)
                    {
                        directoryList.Add(item);
                    }
                }


                return directoryList;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }

        private string GetDirectoryName(string fullPathOfDirectory)
        {
            DirectoryInfo info = new DirectoryInfo(fullPathOfDirectory);

            return info.Name;
        }

        public bool CreateDirectoryFromFtpSite(ref string error, string directory, string localUploadDir)
        {
            //if (isDisposed) return false ;

            bool success = false;
            FileHandler fh = new FileHandler();
            try
            {
                if (Directory.Exists(localUploadDir)) { Directory.Delete(localUploadDir); }

                fh.CreateDirectory(localUploadDir);

                success = true;

                return success;
            }
            catch (Exception ex)
            {
                error = "Directory: " + directory + " - " + ex.Message;
                return success;
            }
        }

        public bool CreateDirectoryFromFileName(ref string error, string newDirectory, string localUploadDir)
        {
            //if (isDisposed) return false ;

            bool success = false;
            FileHandler fh = new FileHandler();
            try
            {
                if (Directory.Exists(localUploadDir + newDirectory)) { Directory.Delete(localUploadDir + newDirectory); }

                fh.CreateDirectory(localUploadDir + newDirectory);

                success = true;

                return success;
            }
            catch (Exception ex)
            {
                error = "Directory: " + newDirectory + " - " + ex.Message;
                return success;
            }
        }

        public bool DeleteDirectory(ref string error, string directory)
        {
            //if (isDisposed) return false ;

            bool success = false;
           
            try
            {
                if (Directory.Exists(directory)) { Directory.Delete(directory); }

                success = true;

                return success;
            }
            catch (Exception ex)
            {
                error = "Cannot Delete Directory: " + directory + " - " + ex.Message;
                return success;
            }
        }

        public bool CreateDirectory(ref string error, string directory)
        {
            //if (isDisposed) return false ;

            bool success = false;

            try
            {
                if (Directory.Exists(directory)) { Directory.Delete(directory); }

                Directory.CreateDirectory(directory);

                success = true;

                return success;
            }
            catch (Exception ex)
            {
                error = "Cannot Create Directory: " + directory + " - " + ex.Message;
                return success;
            }
        }
        public bool CopyFile(ref string error, string copyFromDir, string copyToDir, string file)
        {
            //if (isDisposed) return false ;

            bool success = false;
            FileHandler fh = new FileHandler();

            try
            {
                if (!Directory.Exists(copyFromDir))
                {
                    error = "Copy from directory does not exist!";
                    return success;
                }
                else
                {
                    //Extra layer of protection. This should never happen, but just in case
                    if (!Directory.Exists(copyToDir)) { Directory.CreateDirectory(copyToDir); }

                    FileInfo fileInfo = new FileInfo(copyFromDir + "\\" + file);
                    fileInfo.CopyTo(copyToDir + "\\" + file);

                }
                

                success = true;

                return success;
            }
            catch (Exception ex)
            {
                error = "ERROR: Copy From: " + copyFromDir + ". Copy To: " + copyToDir + ". File Name: " + file + ". Message: " + ex.Message;
                return success;
            }
        }

        public bool DownloadAndSaveFileFromFtpSite(ref string error, ref Ftp ftp, string remoteFtpDirectory, string localUploadDir, string file)
        {
            //if (isDisposed) return false;

            bool success = false;
            FileHandler fh = new FileHandler();

            string ftpFileAndDir = remoteFtpDirectory + "\\" + file;
            string localFileAndDir = localUploadDir + "\\" + file;

            ftp.Download(ref error, ftpFileAndDir, localFileAndDir);

            if (error != string.Empty)
            {
                return false;
            }

            success = true;

            return success;

        }

        public string GetFileNameFromDirectoryString(string pathFile)
        {
            string retVal = string.Empty;

            try
            {
                int lengthOfString = pathFile.Length;
                int lastIndex = pathFile.LastIndexOf('\\');

                retVal = pathFile.Substring(lastIndex + 1, lengthOfString - (lastIndex + 1));

                return retVal;
            }
            catch
            {
                return retVal;
            }

        }
        
        public bool IsDate(string inputDate)
        {
            DateTime dateTime;

            return DateTime.TryParse(inputDate, out dateTime);
        }

        public bool IsWeekday(DateTime inputDate)
        {
            bool isWeekday = false;
                    
            DayOfWeek dow = inputDate.DayOfWeek;

            switch (dow)
            {
                case DayOfWeek.Friday:
                    isWeekday = true;
                    break;
                case DayOfWeek.Monday:
                    isWeekday = true;
                    break;
                case DayOfWeek.Saturday:
                    break;
                case DayOfWeek.Sunday:
                    break;
                case DayOfWeek.Thursday:
                    isWeekday = true;
                    break;
                case DayOfWeek.Tuesday:
                    isWeekday = true;
                    break;
                case DayOfWeek.Wednesday:
                    isWeekday = true;
                    break;
                default:
                    break;
            }

            return isWeekday;
        }

        public List<string> GetFilesFromDirectory(ref string error, string path, string fileExt)
        {

            List<string> fileList = new List<string>();
            FileHandler fh = new FileHandler();
            string[] files;
            string extension = "*." + fileExt;


            try
            {
                if (fh.DirectoryExists(path))
                {
                    files = Directory.GetFiles(path, extension);

                    foreach (string item in files)
                    {
                        fileList.Add(item);
                    }
                }

                return fileList;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
        }

        //#region IDisposable implementation with finalizer   
        //public void Dispose() { Dispose(true); GC.SuppressFinalize(this); }
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!isDisposed)
        //    {
        //        if (disposing)
        //        {
        //            if (this != null) this.Dispose();
        //        }
        //    }
        //    isDisposed = true;
        //}
        //#endregion
    }
}
