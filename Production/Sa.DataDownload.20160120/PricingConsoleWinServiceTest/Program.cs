﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Pricing;
using System.Reflection;
using Solomon.Common.Utility;
using Solomon.Common.Email;



namespace PricingConsoleWinServiceTest
{
    class Program
    {

        static string ftpSite = string.Empty;
        static string ftpUser = string.Empty;
        static string ftpPassword = string.Empty;
        static string ftpDirectory = string.Empty;


        static string rootDrive = string.Empty;
        static string localPricingDir = string.Empty;
        static string processDirectory = string.Empty;
        static string processUploadDirectory = string.Empty;
        static string processDownloadDirectory = string.Empty;
        static string processArchiveDirectory = string.Empty;

        static bool logging = false;

        static string error = string.Empty;

        static void Main(string[] args)
        {

           
            Pricing.Manager pricingMgr = new Pricing.Manager();
            Pricing.Ftp ftp = new Ftp();
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> directoryList = new List<string>();

            try
            {

                bool configsSetPlatts = PopulateProcessVariablesFromConfig("Platts");

                if (configsSetPlatts == false)
                {
                    System.Exception programError = new Exception("PopulateProcessVariables(Platts) FAILED. Cannot continue");
                    LogError("PricingConsoleWinService", "Program", mb.Name, ref programError);
                    return;
                }
                else
                {
                    string plattsWorkingUploadDir = rootDrive + localPricingDir + processDirectory + processUploadDirectory;
                    string latestProcessPlattsDirectory = pricingMgr.GetMostRecentDirectory(rootDrive + localPricingDir + processDirectory + processArchiveDirectory);
                }
               
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }


            //try
            //{
            //    bool configsSet = PopulateProcessVariables("Argus");
            //    pricingMgr.ProcessArgus(ref error, ftpSite, ftpUser, ftpPassword, ftpDirectory, processDirectory, processUploadDirectory);
            //}
            //catch (Exception ex)
            //{
            //    string error = ex.Message;
            //}       
        }


        private bool ProcessPlatts()
        {
            bool success = false;
            Pricing.Manager pricingMgr = new Pricing.Manager();
            Pricing.Ftp ftp = new Ftp();
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> directoryList = new List<string>();

            string response = string.Empty;
            
          

            try
            {
               

                bool isValidPlattsFtpConnection = ftp.IsValidFtpConnection(ref response, ftpSite, ftpUser, ftpPassword);

                if (isValidPlattsFtpConnection)
                {

                    //Get ftp directories and build string list of directory names. Log the step.
                    LogIt("PricingConsoleWinService", "Program", mb.Name, "Started, GetDirectories from Platts.");
                    directoryList = pricingMgr.GetDirectoriesFromFtpSite(ref error, ftpSite, ftpUser, ftpPassword, ftpDirectory, processUploadDirectory);
                    LogIt("PricingConsoleWinService", "Program", mb.Name, "Completed, GetDirectories from Platts. Directory count is : " + Convert.ToString(directoryList.Count));
                    success = true;
                }
                else
                {
                    System.Exception programError = new Exception("Ftp connection for Platts FAILED. Cannot continue.");
                    LogError("PricingConsoleWinService", "Program", mb.Name, ref programError);
                }

                return success;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);                
                throw ex;             
            }

        }


        private bool ProcessArgus()
        {
            bool success = false;
            Pricing.Manager pricingMgr = new Pricing.Manager();
            Pricing.Ftp ftp = new Ftp();
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> directoryList = new List<string>();

            string response = string.Empty;
           

            bool isValidArgusFtpConnection = ftp.IsValidFtpConnection(ref response, ftpSite, ftpUser, ftpPassword);

            try
            {
                bool configsSet = PopulateProcessVariablesFromConfig("Argus");

                if (configsSet == false)
                {
                    System.Exception programError = new Exception("PopulateProcessVariables(Argus) FAILED. Cannot continue.");
                    LogError("PricingConsoleWinService", "Program", mb.Name, ref programError);
                    return false;
                }

                if (isValidArgusFtpConnection)
                {

                    //Get ftp directories and build string list of directory names. Log the step.
                    LogIt("PricingConsoleWinService", "Program", mb.Name, "Started, GetDirectories from Argus.");
                    directoryList = pricingMgr.GetDirectoriesFromFtpSite(ref error, ftpSite, ftpUser, ftpPassword, ftpDirectory, processUploadDirectory);
                    LogIt("PricingConsoleWinService", "Program", mb.Name, "Completed, GetDirectories from Argus. Directory count is : " + Convert.ToString(directoryList.Count));
                    success = true;
                }
                else
                {
                    System.Exception programError = new Exception("Ftp connection for, Argus is invalid. Cannot continue");
                    LogError("PricingConsoleWinService", "Program", mb.Name, ref programError);
                }

                return success;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                throw ex;
            }

        }

        private static bool PopulateProcessVariablesFromConfig(string runType)
        {

            bool retVal = false;

            //Set logging
            if (ConfigurationManager.AppSettings["Logging"] == null) return false;
            logging = Convert.ToBoolean(ConfigurationManager.AppSettings["Logging"]);

            if (ConfigurationManager.AppSettings["RootDriveLetter"] == null) return false;
            if (ConfigurationManager.AppSettings["LocalPricingDir"] == null) return false;
            if (ConfigurationManager.AppSettings["ArchiveDir"] == null) return false;
            if (ConfigurationManager.AppSettings["DownloadDir"] == null) return false;
            if (ConfigurationManager.AppSettings["UploadDir"] == null) return false;

            
            rootDrive = Convert.ToString(ConfigurationManager.AppSettings["RootDriveLetter"]);
            localPricingDir = Convert.ToString(ConfigurationManager.AppSettings["LocalPricingDir"]);
            processArchiveDirectory = Convert.ToString(ConfigurationManager.AppSettings["ArchiveDir"]);
            processUploadDirectory = Convert.ToString(ConfigurationManager.AppSettings["UploadDir"]);      
            processDownloadDirectory = Convert.ToString(ConfigurationManager.AppSettings["DownloadDir"]);
            
                    
            if (runType == "Argus")
            {

                //FTP Argus
                if (ConfigurationManager.AppSettings["FtpSiteArgus"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusUser"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusPwd"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusDir"] == null) return false;

                if (ConfigurationManager.AppSettings["ArgusDir"] == null) return false;

                ftpSite = Convert.ToString(ConfigurationManager.AppSettings["FtpSiteArgus"]);
                ftpUser = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusUser"]);
                ftpPassword = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusPwd"]);
                ftpDirectory = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusDir"]);
                
                processDirectory = Convert.ToString(ConfigurationManager.AppSettings["ArgusDir"]);
               
                
                retVal = true;
                
            }


            if (runType == "Platts")
            {
                //FTP Platts
                if (ConfigurationManager.AppSettings["FtpSitePlatts"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsUser"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsPwd"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsDir"] == null) return false;
               

                ftpSite = Convert.ToString(ConfigurationManager.AppSettings["FtpSitePlatts"]);
                ftpUser = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsUser"]);
                ftpPassword = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsPwd"]);
                ftpDirectory = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsDir"]);
                
                processDirectory = Convert.ToString(ConfigurationManager.AppSettings["PlattsDir"]);
               

                retVal = true;
            }

            return retVal;
        }


        private static void SendEmail(bool isError, string content)
        {
            MethodBase mb = MethodBase.GetCurrentMethod();
            string appSetting = string.Empty;

            try
            {

                if (IsValidEmailCredentials(ref appSetting))
               
                {
                    Solomon.Common.Email.Manager emailMgr = new Solomon.Common.Email.Manager();


                    if (isError)
                    {
                        emailMgr.SendEmail(Convert.ToString(ConfigurationManager.AppSettings["EmailFrom"]),
                                                                    Convert.ToString(ConfigurationManager.AppSettings["EmailTo"]),
                                                                        Convert.ToString(ConfigurationManager.AppSettings["EmailAppErrorSubject"]),
                                                                            content, Convert.ToString(ConfigurationManager.AppSettings["SmtpServer"]));
                    
                    }
                    else
                    {
                        emailMgr.SendEmail(Convert.ToString(ConfigurationManager.AppSettings["EmailFrom"]),
                                                                   Convert.ToString(ConfigurationManager.AppSettings["EmailTo"]),
                                                                       Convert.ToString(ConfigurationManager.AppSettings["EmailAppSubject"]),
                                                                           content, Convert.ToString(ConfigurationManager.AppSettings["SmtpServer"]));
                    }                                                             
                }
                else
                {
                    System.Exception programError = new Exception(appSetting +  " string is null in configuration. Email will not be sent.");
                    LogError("PricingConsoleWinService", "Program", mb.Name, ref programError);
                    return;
                }

            }
            catch (Exception ex)
            {            
                LogError("PricingConsoleWinService", "Program", mb.Name, ref ex);
            }
        }

        private static void LogIt(string thisAssembly, string thisClass, string thisMethod, string logText)
        {
            if (logging == true)
            {
                Log.Instance().Info("LOG: ", "Assembly: " + thisAssembly + " |Class: " + thisClass + " |Method: " + thisMethod + " |Message: " + logText);
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"])) { SendEmail(false, logText); }
            }

        }

        private static void LogError(string thisAssembly, string thisClass, string thisMethod, ref System.Exception ex)
        {
            if (logging == true)
            {
                Log.Instance().Error("ERROR: Assembly: " + thisAssembly + " |Class: " + thisClass + " |Method: " + thisMethod, ex);
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmailWithError"])) { SendEmail(true, ex.Message + Environment.NewLine + ex.StackTrace); }
            }
          
        }

        private static bool IsValidEmailCredentials(ref string appSetting)
        {

            if (ConfigurationManager.AppSettings["SendEmail"] == null) { appSetting = "SendEmail"; return false; }
            if (ConfigurationManager.AppSettings["SendEmailWithError"] == null) { appSetting = "SendEmailWithError"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServer"] == null) { appSetting = "SmtpServer"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServerPort"] == null) { appSetting = "SmtpServerPort"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServerUser"] == null) { appSetting = "SmtpServerUser"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServerUserPassword"] == null) { appSetting = "SmtpServerUserPassword"; return false; }
            if (ConfigurationManager.AppSettings["EmailTo"] == null) { appSetting = "EmailTo"; return false; }
            if (ConfigurationManager.AppSettings["EmailCc"] == null) { appSetting = "EmailCc"; return false; }
            if (ConfigurationManager.AppSettings["EmailFrom"] == null) { appSetting = "EmailFrom"; return false; }
            if (ConfigurationManager.AppSettings["EmailAppErrorSubject"] == null) { appSetting = "EmailAppErrorSubject"; return false; }
            if (ConfigurationManager.AppSettings["EmailAppSubject"] == null) { appSetting = "EmailAppSubject"; return false; }
            if (ConfigurationManager.AppSettings["EmailAppOwner"] == null) { appSetting = "EmailAppOwner"; return false; }

            return true;
        }

    }
}
