﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingFtp.Classes
{
    public class Argus
    {

        public bool IsValidFileType(string value)
        {
          
                if (value.Trim().EndsWith(".csv"))
                {
                    return true;
                }
                else
                {
                    return false;
                }      
        }
        
        public bool IsValidFileToProcess(string value, int length, int startIndex, int endIndex)
        {
            try
            {
                string checkValue = value.Substring(startIndex, endIndex);
                int i = Convert.ToInt32(checkValue.ToString());
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }


      
    }
}
