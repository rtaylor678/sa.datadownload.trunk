﻿CREATE TABLE [dim].[DS_DataSource_Bridge]
(
	[DS_MethodologyId]		INT					NOT	NULL,

	[DS_DataSourceId]		INT					NOT NULL	CONSTRAINT [FK__DS_DataSource_Bridge_DS_DataSourceId]					REFERENCES [dim].[DS_DataSource_LookUp]([DS_DataSourceId])
															CONSTRAINT [FK__DS_DataSource_Bridge_DS_DataSource_Parent_Ancestor]		FOREIGN KEY ([DS_MethodologyId], [DS_DataSourceId])
																																	REFERENCES [dim].[DS_DataSource_Parent]([DS_MethodologyId], [DS_DataSourceId]),

	[DS_DescendantId]		INT					NOT NULL	CONSTRAINT [FK__DS_DataSource_Bridge_DS_DescendantId]					REFERENCES [dim].[DS_DataSource_LookUp]([DS_DataSourceId])
															CONSTRAINT [FK__DS_DataSource_Bridge_DS_DataSource_Parent_Descendant]	FOREIGN KEY ([DS_MethodologyId], [DS_DataSourceId])
																																	REFERENCES [dim].[DS_DataSource_Parent]([DS_MethodologyId], [DS_DataSourceId]),

	[DS_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__DS_DataSource_Bridge_DS_PositedBy]						DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__DS_DataSource_Bridge_DS_PositedBy]						REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[DS_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__DS_DataSource_Bridge_DS_PositedAt]						DEFAULT(SYSDATETIMEOFFSET()),
	[DS_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__DS_DataSource_Bridge_DS_RowGuid]						DEFAULT(newsequentialid())	ROWGUIDCOL,
															CONSTRAINT [UX__DS_DataSource_Bridge_DS_RowGuid]						UNIQUE NONCLUSTERED([DS_RowGuid]),

	CONSTRAINT [PK__DS_DataSource_Bridge]	PRIMARY KEY CLUSTERED([DS_MethodologyId] ASC, [DS_DataSourceId] ASC, [DS_DescendantId] ASC)
);