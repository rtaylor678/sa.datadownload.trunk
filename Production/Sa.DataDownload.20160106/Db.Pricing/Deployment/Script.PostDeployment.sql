﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET NOCOUNT ON;

:r .\Post\posit\insert.posit.Positor.sql
:r .\Post\dim\insert.dim.Operator.sql

:r .\Seed\ArgusCsv\insert.Argus.reference.sql
:r .\Seed\PlattsFtp\insert.Platts.reference.sym.sql

:r .\Post\dim\insert.dim.Currency.sql
:r .\Post\dim\insert.dim.Uom.sql
:r .\Post\dim\insert.dim.DataSource.sql
:r .\Post\dim\insert.dim.Location.sql
:r .\Post\dim\insert.dim.Commodity.sql

:r .\Post\etl\insert.etl.Argus.sql
:r .\Post\etl\insert.etl.Platts.sql

--:r .\Seed\ArgusCsv\insert.Argus.seed.sql
--:r .\Seed\PlattsFtp\insert.Platts.seed.sql
