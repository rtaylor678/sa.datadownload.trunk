﻿CREATE VIEW [fact].[PR_PricingCommodity_Current]
WITH SCHEMABINDING
AS
SELECT
	[j].[PR_ID],
	[j].[PR_CommodityId],
	[j].[PR_PriceDate],
	[j].[PR_CurrencyId],
	[j].[PR_UomId],
	[j].[PR_DataSourceId],

	[j].[PR_VAL_ID],
	[j].[PR_VAL_PR_ID],
	[j].[PR_VAL_Open],
	[j].[PR_VAL_Close],
	[j].[PR_VAL_Low],
	[j].[PR_VAL_High],
	[j].[PR_VAL_Index],
	[j].[PR_VAL_Ask],
	[j].[PR_VAL_Bid],
	[j].[PR_VAL_Mean],
	[j].[PR_VAL_PrevInterest],
	[j].[PR_VAL_Latest],
	[j].[PR_VAL_Change],
	[j].[PR_VAL_Volume],
	[j].[PR_VAL_ChangedAt],
	[j].[PR_VAL_PositedBy],
	[j].[PR_VAL_PositedAt],
	[j].[PR_VAL_PositReliability],
	[j].[PR_VAL_PositReliable]

FROM [fact].[PR_PricingCommodity_Interval](DEFAULT, DEFAULT, SYSDATETIMEOFFSET(), 1)	[j]
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Current_Insert]
ON [fact].[PR_PricingCommodity_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PositedAt	DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy	INT;
	EXECUTE @PositedBy	= [posit].[Return_PositorId];

	INSERT INTO [fact].[PR_PricingCommodity]
	(
		[PR_PriceDate],
		[PR_CommodityId],
		[PR_CurrencyId],
		[PR_UomId],
		[PR_DataSourceId]
	)
	SELECT DISTINCT
		[i].[PR_PriceDate],
		[i].[PR_CommodityId],
		[i].[PR_CurrencyId],
		[i].[PR_UomId],
		[i].[PR_DataSourceId]
	FROM
		[inserted]											[i]
	LEFT OUTER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	WHERE	[t].[PR_ID]	IS NULL;

	INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Posit]
	(
		[PR_VAL_PR_ID],
		[PR_VAL_Open],
		[PR_VAL_Close],
		[PR_VAL_Low],
		[PR_VAL_High],
		[PR_VAL_Index],
		[PR_VAL_Ask],
		[PR_VAL_Bid],
		[PR_VAL_Mean],
		[PR_VAL_PrevInterest],
		[PR_VAL_Latest],
		[PR_VAL_Change],
		[PR_VAL_Volume],
		[PR_VAL_ChangedAt]
	)
	SELECT
		[t].[PR_ID],
		[i].[PR_VAL_Open],
		[i].[PR_VAL_Close],
		[i].[PR_VAL_Low],
		[i].[PR_VAL_High],
		[i].[PR_VAL_Index],
		[i].[PR_VAL_Ask],
		[i].[PR_VAL_Bid],
		[i].[PR_VAL_Mean],
		[i].[PR_VAL_PrevInterest],
		[i].[PR_VAL_Latest],
		[i].[PR_VAL_Change],
		[i].[PR_VAL_Volume],
			[PR_VAL_ChangedAt]	= COALESCE([i].[PR_VAL_ChangedAt], @PositedAt)
	FROM
		[inserted]											[i]
	INNER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	LEFT OUTER JOIN
		[fact].[PR_VAL_PricingCommodity_Value_Posit]		[x]
			ON	[x].[PR_VAL_PR_ID]		= [t].[PR_ID]
			AND	[x].[PR_VAL_ChangedAt]	= COALESCE([i].[PR_VAL_ChangedAt], @PositedAt)
			AND	[x].[PR_VAL_Checksum]	= BINARY_CHECKSUM([i].[PR_VAL_Open], [i].[PR_VAL_Close], [i].[PR_VAL_Low], [i].[PR_VAL_High], [i].[PR_VAL_Index], [i].[PR_VAL_Ask], [i].[PR_VAL_Bid], [i].[PR_VAL_Mean], [i].[PR_VAL_Volume])
	WHERE
		[x].[PR_VAL_ID]	IS NULL;

	INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Annex]
	(
		[PR_VAL_ID],
		[PR_VAL_PositedBy],
		[PR_VAL_PositedAt],
		[PR_VAL_PositReliability]
	)
	SELECT
		[p].[PR_VAL_ID],
			[PR_VAL_PositedBy]	= CASE WHEN (UPDATE([PR_VAL_PositedBy])) THEN [i].[PR_VAL_PositedBy] ELSE @PositedBy END,
			[PR_VAL_PositedAt]	= CASE WHEN (UPDATE([PR_VAL_PositedAt])) THEN [i].[PR_VAL_PositedAt] ELSE @PositedAt END,
		[i].[PR_VAL_PositReliability]
	FROM
		[inserted]											[i]
	INNER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	INNER JOIN
		[fact].[PR_VAL_PricingCommodity_Value_Posit]		[p]
			ON	[p].[PR_VAL_PR_ID]		= [t].[PR_ID]
			AND	[p].[PR_VAL_ChangedAt]	= COALESCE([i].[PR_VAL_ChangedAt], @PositedAt)
	LEFT OUTER JOIN
		[fact].[PR_VAL_PricingCommodity_Value_Annex]		[x]
			ON	[x].[PR_VAL_ID]			= [p].[PR_VAL_ID]
			AND	[x].[PR_VAL_PositedBy]	= COALESCE([i].[PR_VAL_PositedBy], @PositedBy)
			AND	[x].[PR_VAL_PositedAt]	= COALESCE([i].[PR_VAL_PositedAt], @PositedAt)
	WHERE
		[x].[PR_VAL_ID]	IS NULL;

END;
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Current_Update]
ON [fact].[PR_PricingCommodity_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	IF(UPDATE([PR_ID]))
		RAISERROR('The identity column [PR_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_VAL_ID]))
		RAISERROR('The identity column [PR_VAL_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_VAL_PR_ID]))
		RAISERROR('The identity column [PR_VAL_PR_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_CommodityId]))
		RAISERROR('The foreign key identity column [PR_CommodityId] is not updatable.', 16, 1);

	IF(UPDATE([PR_PriceDate]))
		RAISERROR('The foreign key identity column [PR_PriceDate] is not updatable.', 16, 1);

	IF(UPDATE([PR_CurrencyId]))
		RAISERROR('The foreign key identity column [PR_CurrencyId] is not updatable.', 16, 1);

	IF(UPDATE([PR_UomId]))
		RAISERROR('The foreign key identity column [PR_UomId] is not updatable.', 16, 1);

	IF(UPDATE([PR_DataSourceId]))
		RAISERROR('The foreign key identity column [PR_DataSourceId] is not updatable.', 16, 1);

	IF(UPDATE([PR_VAL_PositReliable]))
		RAISERROR('The computed column [PR_VAL_PositReliable] is not updatable.', 16, 1);

	DECLARE @PositedAt	DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy	INT;
	EXECUTE @PositedBy	= [posit].[Return_PositorId];

	IF (UPDATE([PR_VAL_Open])			OR
		UPDATE([PR_VAL_Close])			OR
		UPDATE([PR_VAL_Low])			OR
		UPDATE([PR_VAL_High])			OR
		UPDATE([PR_VAL_Index])			OR
		UPDATE([PR_VAL_Ask])			OR
		UPDATE([PR_VAL_Bid])			OR
		UPDATE([PR_VAL_Mean])			OR
		UPDATE([PR_VAL_PrevInterest])	OR
		UPDATE([PR_VAL_Latest])			OR
		UPDATE([PR_VAL_Change])			OR
		UPDATE([PR_VAL_Volume])			OR
		UPDATE([PR_VAL_ChangedAt]))
	BEGIN

		INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Posit]
		(
			[PR_VAL_PR_ID],
			[PR_VAL_Open],
			[PR_VAL_Close],
			[PR_VAL_Low],
			[PR_VAL_High],
			[PR_VAL_Index],
			[PR_VAL_Ask],
			[PR_VAL_Bid],
			[PR_VAL_Mean],
			[PR_VAL_PrevInterest],
			[PR_VAL_Latest],
			[PR_VAL_Change],
			[PR_VAL_Volume],
			[PR_VAL_ChangedAt]
		)
		SELECT
			[i].[PR_ID],
			[i].[PR_VAL_Open],
			[i].[PR_VAL_Close],
			[i].[PR_VAL_Low],
			[i].[PR_VAL_High],
			[i].[PR_VAL_Index],
			[i].[PR_VAL_Ask],
			[i].[PR_VAL_Bid],
			[i].[PR_VAL_Mean],
			[i].[PR_VAL_PrevInterest],
			[i].[PR_VAL_Latest],
			[i].[PR_VAL_Change],
			[i].[PR_VAL_Volume],
			[i].[PR_VAL_ChangedAt]
		FROM
			[inserted]											[i]
		LEFT OUTER JOIN
			[fact].[PR_VAL_PricingCommodity_Value_Posit]		[x]
				ON	[x].[PR_VAL_PR_ID]		= [i].[PR_ID]
				AND	[x].[PR_VAL_ChangedAt]	= [i].[PR_VAL_ChangedAt]
				AND	[x].[PR_VAL_Checksum]	= BINARY_CHECKSUM([i].[PR_VAL_Open], [i].[PR_VAL_Close], [i].[PR_VAL_Low], [i].[PR_VAL_High], [i].[PR_VAL_Index], [i].[PR_VAL_Ask], [i].[PR_VAL_Bid], [i].[PR_VAL_Mean], [i].[PR_VAL_Volume])
		WHERE
			[x].[PR_VAL_ID]	IS NULL;

		INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Annex]
		(
			[PR_VAL_ID],
			[PR_VAL_PositedBy],
			[PR_VAL_PositedAt],
			[PR_VAL_PositReliability]
		)
		SELECT
			[p].[PR_VAL_ID],
				[PR_VAL_PositedBy]	= CASE WHEN (UPDATE([PR_VAL_PositedBy])) THEN [i].[PR_VAL_PositedBy] ELSE @PositedBy END,
				[PR_VAL_PositedAt]	= CASE WHEN (UPDATE([PR_VAL_PositedAt])) THEN [i].[PR_VAL_PositedAt] ELSE @PositedAt END,
			[i].[PR_VAL_PositReliability]
		FROM
			[inserted]										[i]
		INNER JOIN
			[fact].[PR_VAL_PricingCommodity_Value_Posit]	[p]
				ON	[p].[PR_VAL_PR_ID]		= [i].[PR_ID]
				AND	[p].[PR_VAL_ChangedAt]	= [i].[PR_VAL_ChangedAt]
		LEFT OUTER JOIN
			[fact].[PR_VAL_PricingCommodity_Value_Annex]	[a]
				ON	[a].[PR_VAL_ID]			= [p].[PR_VAL_ID]
		WHERE	[a].[PR_VAL_ID] IS NULL;

	END;

END;
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Current_Delete]
ON [fact].[PR_PricingCommodity_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy	INT;
	EXECUTE @PositedBy	= [posit].[Return_PositorId];

	INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Annex]
	(
		[PR_VAL_ID],
		[PR_VAL_PositedBy],
		[PR_VAL_PositedAt],
		[PR_VAL_PositReliability]
	)
	SELECT
		[d].[PR_VAL_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d];

END;
GO