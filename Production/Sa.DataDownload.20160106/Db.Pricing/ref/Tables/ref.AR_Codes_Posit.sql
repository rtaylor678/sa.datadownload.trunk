﻿CREATE TABLE [ref].[AR_Codes_Posit]
(
	[AR_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[AR_Code]					CHAR(9)				NOT	NULL	CONSTRAINT [CL__AR_Codes_Posit_AR_Code]				CHECK([AR_Code] <> ''),
	[AR_DisplayName]			VARCHAR(128)		NOT	NULL	CONSTRAINT [CL__AR_Codes_Posit_AR_DisplayName]		CHECK([AR_DisplayName] <> ''),
	[AR_DeliveryMode]			VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__AR_Codes_Posit_AR_DeliveryMode]		CHECK([AR_DeliveryMode] <> ''),
	[AR_Unit]					VARCHAR(16)			NOT	NULL	CONSTRAINT [CL__AR_Codes_Posit_AR_Unit]				CHECK([AR_Unit] <> ''),
	[AR_Frequency]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__AR_Codes_Posit_AR_Frequency]		CHECK([AR_Frequency] <> ''),

	[AR_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_Codes_Posit_AR_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__AR_Codes_Posit_AR_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AR_Codes_Posit_AR_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AR_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_Codes_Posit_AR_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__AR_Codes_Posit_AR_Reliability]		DEFAULT(50),
	[AR_PositReliable]			AS CONVERT(BIT, CASE WHEN [AR_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[AR_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AR_Codes_Posit_AR_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__AR_Codes_Posit_AR__RowGuid]			UNIQUE NONCLUSTERED([AR_RowGuid]),

	CONSTRAINT [PK__AR_Codes_Posit] PRIMARY KEY([AR_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__AR_Codes_Posit] UNIQUE CLUSTERED([AR_Code] ASC, [AR_DeliveryMode] ASC)
		WITH(FILLFACTOR = 95)
);