﻿-- POSITOR METADATA ---------------------------------------------------------------------------------------------------
--
-- Sets up a table containing the list of available positors. Since at least one positor
-- must be available the table is set up with a default positor with identity 0.
--
-- Positor table ------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Positor', 'U') IS NULL
BEGIN
    CREATE TABLE [dbo].[_Positor] (
        Positor tinyint not null,
        constraint pk_Positor primary key (
            Positor asc
        )
    );
    INSERT INTO [dbo].[_Positor] (
        Positor
    )
    VALUES (
        0 -- the default positor
    );
END
GO
-- KNOTS --------------------------------------------------------------------------------------------------------------
--
-- Knots are used to store finite sets of values, normally used to describe states
-- of entities (through knotted attributes) or relationships (through knotted ties).
-- Knots have their own surrogate identities and are therefore immutable.
-- Values can be added to the set over time though.
-- Knots should have values that are mutually exclusive and exhaustive.
--
-- ANCHORS AND ATTRIBUTES ---------------------------------------------------------------------------------------------
--
-- Anchors are used to store the identities of entities.
-- Anchors are immutable.
-- Attributes are used to store values for properties of entities.
-- Attributes are mutable, their values may change over one or more types of time.
-- Attributes have four flavors: static, historized, knotted static, and knotted historized.
-- Anchors may have zero or more adjoined attributes.
--
-- Anchor table -------------------------------------------------------------------------------------------------------
-- PR_Pricing table (with 1 attributes)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PR_Pricing', 'U') IS NULL
CREATE TABLE [dbo].[PR_PricingCommodity] (
    PR_ID int not null,
    PR_Dummy bit null,
    constraint pkPR_Pricing primary key (
        PR_ID asc
    )
);
GO
-- Historized attribute posit table -----------------------------------------------------------------------------------
-- PR_VAL_PricingCommodity_Index_Posit table (on PR_Pricing)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PR_VAL_PricingCommodity_Index_Posit', 'U') IS NULL
CREATE TABLE [dbo].[PR_VAL_PricingCommodity_Index_Posit] (
    PR_VAL_ID int not null,
    PR_VAL_PR_ID int not null,
    PR_VAL_PricingCommodity_Index FLOAT not null,
    PR_VAL_ChangedAt datetime not null,
    constraint fkPR_VAL_PricingCommodity_Index_Posit foreign key (
        PR_VAL_PR_ID
    ) references [dbo].[PR_PricingCommodity](PR_ID),
    constraint pkPR_VAL_PricingCommodity_Index_Posit primary key nonclustered (
        PR_VAL_ID asc
    ),
    constraint uqPR_VAL_PricingCommodity_Index_Posit unique clustered (
        PR_VAL_PR_ID asc,
        PR_VAL_ChangedAt desc,
        PR_VAL_PricingCommodity_Index asc
    )
);
GO
-- Attribute annex table ----------------------------------------------------------------------------------------------
-- PR_VAL_PricingCommodity_Index_Annex table (of PR_VAL_PricingCommodity_Index_Posit on PR_Pricing)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PR_VAL_PricingCommodity_Index_Annex', 'U') IS NULL
CREATE TABLE [dbo].[PR_VAL_PricingCommodity_Index_Annex] (
    PR_VAL_ID int not null,
    PR_VAL_PositedAt datetime not null,
    PR_VAL_Positor tinyint not null,
    PR_VAL_Reliability decimal(5,2) not null,
    PR_VAL_Assertion as isnull(cast(
        case
            when PR_VAL_Reliability > 0 then '+'
            when PR_VAL_Reliability < 0 then '-'
            else '?'
        end
    as char(1)), '?') persisted,
    constraint fkPR_VAL_PricingCommodity_Index_Annex foreign key (
        PR_VAL_ID
    ) references [dbo].[PR_VAL_PricingCommodity_Index_Posit](PR_VAL_ID),
    constraint pkPR_VAL_PricingCommodity_Index_Annex primary key clustered (
        PR_VAL_ID asc,
        PR_VAL_Positor asc,
        PR_VAL_PositedAt desc
    )
);
GO
-- ATTRIBUTE ASSEMBLED VIEWS ------------------------------------------------------------------------------------------
--
-- The assembled view of an attribute combines the posit and annex table of the attribute.
-- It can be used to maintain entity integrity through a primary key, which cannot be
-- defined elsewhere.
--
-- Attribute assembled view -------------------------------------------------------------------------------------------
-- PR_VAL_PricingCommodity_Index assembled view of the posit and annex tables,
-- pkPR_VAL_PricingCommodity_Index optional temporal consistency constraint
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PR_VAL_PricingCommodity_Index', 'V') IS NULL
BEGIN
    EXEC('
    CREATE VIEW [dbo].[PR_VAL_PricingCommodity_Index]
    WITH SCHEMABINDING AS
    SELECT
        p.PR_VAL_ID,
        p.PR_VAL_PR_ID,
        p.PR_VAL_PricingCommodity_Index,
        p.PR_VAL_ChangedAt,
        a.PR_VAL_PositedAt,
        a.PR_VAL_Positor,
        a.PR_VAL_Reliability,
        a.PR_VAL_Assertion
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index_Posit] p
    JOIN
        [dbo].[PR_VAL_PricingCommodity_Index_Annex] a
    ON
        a.PR_VAL_ID = p.PR_VAL_ID;
    ');
END
GO
-- ATTRIBUTE REWINDERS AND FORWARDERS ---------------------------------------------------------------------------------
--
-- These table valued functions rewind an attribute posit table to the given
-- point in changing time, or an attribute annex table to the given point
-- in positing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time. The forwarder is the opposite of the rewinder, such that the 
-- union of the two will produce all rows in a posit table.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to rewind to (defaults to End of Time, no rewind)
-- @positingTimepoint the point in positing time to rewind to (defaults to End of Time, no rewind)
--
-- Attribute posit rewinder -------------------------------------------------------------------------------------------
-- rPR_VAL_PricingCommodity_Index_Posit rewinding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rPR_VAL_PricingCommodity_Index_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rPR_VAL_PricingCommodity_Index_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        PR_VAL_ID,
        PR_VAL_PR_ID,
        PR_VAL_PricingCommodity_Index,
        PR_VAL_ChangedAt
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index_Posit]
    WHERE
        PR_VAL_ChangedAt <= @changingTimepoint;
    ');
END
GO
-- Attribute posit forwarder ------------------------------------------------------------------------------------------
-- fPR_VAL_PricingCommodity_Index_Posit forwarding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fPR_VAL_PricingCommodity_Index_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fPR_VAL_PricingCommodity_Index_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        PR_VAL_ID,
        PR_VAL_PR_ID,
        PR_VAL_PricingCommodity_Index,
        PR_VAL_ChangedAt
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index_Posit]
    WHERE
        PR_VAL_ChangedAt > @changingTimepoint;
    ');
END
GO
-- Attribute annex rewinder -------------------------------------------------------------------------------------------
-- rPR_VAL_PricingCommodity_Index_Annex rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rPR_VAL_PricingCommodity_Index_Annex','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rPR_VAL_PricingCommodity_Index_Annex] (
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        PR_VAL_ID,
        PR_VAL_PositedAt,
        PR_VAL_Positor,
        PR_VAL_Reliability,
        PR_VAL_Assertion
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index_Annex]
    WHERE
        PR_VAL_PositedAt <= @positingTimepoint;
    ');
END
GO
-- Attribute assembled rewinder ---------------------------------------------------------------------------------------
-- rPR_VAL_PricingCommodity_Index rewinding over changing and positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rPR_VAL_PricingCommodity_Index','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rPR_VAL_PricingCommodity_Index] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.PR_VAL_ID,
        a.PR_VAL_PositedAt,
        a.PR_VAL_Positor,
        a.PR_VAL_Reliability,
        a.PR_VAL_Assertion,
        p.PR_VAL_PR_ID,
        p.PR_VAL_PricingCommodity_Index,
        p.PR_VAL_ChangedAt
    FROM
        [dbo].[rPR_VAL_PricingCommodity_Index_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rPR_VAL_PricingCommodity_Index_Annex](@positingTimepoint) a
    ON
        a.PR_VAL_ID = p.PR_VAL_ID
    AND
        a.PR_VAL_Positor = @positor
    AND
        a.PR_VAL_PositedAt = (
            SELECT TOP 1
                sub.PR_VAL_PositedAt
            FROM
                [dbo].[rPR_VAL_PricingCommodity_Index_Annex](@positingTimepoint) sub
            WHERE
                sub.PR_VAL_ID = p.PR_VAL_ID
            AND
                sub.PR_VAL_Positor = @positor
            ORDER BY
                sub.PR_VAL_PositedAt DESC
        )
    ');
END
GO
-- Attribute assembled forwarder --------------------------------------------------------------------------------------
-- fPR_VAL_PricingCommodity_Index forwarding over changing and rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fPR_VAL_PricingCommodity_Index','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fPR_VAL_PricingCommodity_Index] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.PR_VAL_ID,
        a.PR_VAL_PositedAt,
        a.PR_VAL_Positor,
        a.PR_VAL_Reliability,
        a.PR_VAL_Assertion,
        p.PR_VAL_PR_ID,
        p.PR_VAL_PricingCommodity_Index,
        p.PR_VAL_ChangedAt
    FROM
        [dbo].[fPR_VAL_PricingCommodity_Index_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rPR_VAL_PricingCommodity_Index_Annex](@positingTimepoint) a
    ON
        a.PR_VAL_ID = p.PR_VAL_ID
    AND
        a.PR_VAL_Positor = @positor
    AND
        a.PR_VAL_PositedAt = (
            SELECT TOP 1
                sub.PR_VAL_PositedAt
            FROM
                [dbo].[rPR_VAL_PricingCommodity_Index_Annex](@positingTimepoint) sub
            WHERE
                sub.PR_VAL_ID = p.PR_VAL_ID
            AND
                sub.PR_VAL_Positor = @positor
            ORDER BY
                sub.PR_VAL_PositedAt DESC
        )
    ');
END
GO
-- Attribute previous value -------------------------------------------------------------------------------------------
-- prePR_VAL_PricingCommodity_Index function for getting previous value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.prePR_VAL_PricingCommodity_Index','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[prePR_VAL_PricingCommodity_Index] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31'', 
        @assertion char(1) = null
    )
    RETURNS FLOAT
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.PR_VAL_PricingCommodity_Index
        FROM
            [dbo].[rPR_VAL_PricingCommodity_Index](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.PR_VAL_PR_ID = @id
        AND
            pre.PR_VAL_ChangedAt < @changingTimepoint
        AND
            pre.PR_VAL_Assertion = isnull(@assertion, pre.PR_VAL_Assertion)
        ORDER BY
            pre.PR_VAL_ChangedAt DESC,
            pre.PR_VAL_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute following value ------------------------------------------------------------------------------------------
-- folPR_VAL_PricingCommodity_Index function for getting following value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.folPR_VAL_PricingCommodity_Index','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[folPR_VAL_PricingCommodity_Index] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31'', 
        @assertion char(1) = null
    )
    RETURNS FLOAT
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.PR_VAL_PricingCommodity_Index
        FROM
            [dbo].[fPR_VAL_PricingCommodity_Index](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.PR_VAL_PR_ID = @id
        AND
            fol.PR_VAL_ChangedAt > @changingTimepoint
        AND
            fol.PR_VAL_Assertion = isnull(@assertion, fol.PR_VAL_Assertion)
        ORDER BY
            fol.PR_VAL_ChangedAt ASC,
            fol.PR_VAL_PositedAt DESC
    );
    END
    ');
END
GO
-- ATTRIBUTE RESTATEMENT CONSTRAINTS ----------------------------------------------------------------------------------
--
-- Attributes may be prevented from storing restatements.
-- A restatement is when the same value occurs for two adjacent points
-- in changing time. Note that restatement checking is not done for
-- unreliable information as this could prevent demotion.
--
-- returns 1 for at least one equal surrounding value, 0 for different surrounding values
--
-- @posit the id of the posit that should be checked
-- @posited the time when this posit was made
-- @positor the one who made the posit
-- @assertion whether this posit is positively or negatively asserted, or unreliable
--
-- Restatement Finder Function and Constraint -------------------------------------------------------------------------
-- rfPR_VAL_PricingCommodity_Index restatement finder, also used by the insert and update triggers for idempotent attributes
-- rcPR_VAL_PricingCommodity_Index restatement constraint (available only in attributes that cannot have restatements)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rfPR_VAL_PricingCommodity_Index', 'FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rfPR_VAL_PricingCommodity_Index] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @assertion char(1)
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value FLOAT;
    DECLARE @changed datetime;
    SELECT
        @id = PR_VAL_PR_ID,
        @value = PR_VAL_PricingCommodity_Index,
        @changed = PR_VAL_ChangedAt
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index_Posit]
    WHERE
        PR_VAL_ID = @posit;
    RETURN (
        CASE
        WHEN @assertion = ''?''
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = [dbo].[prePR_VAL_PricingCommodity_Index] (
                    @id,
                    @positor,
                    @changed,
                    @posited, 
                    @assertion
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = [dbo].[folPR_VAL_PricingCommodity_Index] (
                    @id,
                    @positor,
                    @changed,
                    @posited, 
                    @assertion
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    ');
END
GO
-- ANCHOR TEMPORAL PERSPECTIVES ---------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each anchor. There are five types of perspectives: time traveling, latest,
-- point-in-time, difference, and now. They also denormalize the anchor, its attributes,
-- and referenced knots from sixth to third normal form.
--
-- The time traveling perspective shows information as it was or will be based on a number
-- of input parameters.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to travel to (defaults to End of Time)
-- @positingTimepoint the point in positing time to travel to (defaults to End of Time)
-- @assertion whether to show positve, negative, uncertain, or all posits (defaults to all)
--
-- The latest perspective shows the latest available (changing & positing) information for each anchor.
-- The now perspective shows the information as it is right now, with latest positing time.
-- The point-in-time perspective lets you travel through the information to the given timepoint,
-- with latest positing time and the given point in changing time.
--
-- @changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints, and for
-- changes in all or a selection of attributes, with latest positing time.
--
-- @intervalStart the start of the interval for finding changes
-- @intervalEnd the end of the interval for finding changes
-- @selection a list of mnemonics for tracked attributes, ie 'MNE MON ICS', or null for all
--
-- Drop perspectives --------------------------------------------------------------------------------------------------
IF Object_ID('dbo.dPR_Pricing', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[dPR_PricingCommodity];
IF Object_ID('dbo.nPR_Pricing', 'V') IS NOT NULL
DROP VIEW [dbo].[nPR_PricingCommodity];
IF Object_ID('dbo.pPR_Pricing', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[pPR_PricingCommodity];
IF Object_ID('dbo.lPR_Pricing', 'V') IS NOT NULL
DROP VIEW [dbo].[lPR_PricingCommodity];
IF Object_ID('dbo.tPR_Pricing', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[tPR_PricingCommodity];
GO
-- Time traveling perspective -----------------------------------------------------------------------------------------
-- tPR_Pricing viewed as given by the input parameters
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[tPR_PricingCommodity] (
    @positor tinyint = 0,
    @changingTimepoint datetime2(7) = '9999-12-31',
    @positingTimepoint datetime = '9999-12-31',
    @assertion char(1) = null
)
RETURNS TABLE WITH SCHEMABINDING AS RETURN
SELECT
    [PR].PR_ID,
    [VAL].PR_VAL_PR_ID,
    [VAL].PR_VAL_ID,
    [VAL].PR_VAL_ChangedAt,
    [VAL].PR_VAL_PositedAt,
    [VAL].PR_VAL_Positor,
    [VAL].PR_VAL_Reliability,
    [VAL].PR_VAL_Assertion,
    [VAL].PR_VAL_PricingCommodity_Index
FROM
    [dbo].[PR_PricingCommodity] [PR]
LEFT JOIN
    [dbo].[rPR_VAL_PricingCommodity_Index](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [VAL]
ON
    [VAL].PR_VAL_ID = (
        SELECT TOP 1
            sub.PR_VAL_ID
        FROM
            [dbo].[rPR_VAL_PricingCommodity_Index](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.PR_VAL_PR_ID = [PR].PR_ID
        AND
            sub.PR_VAL_Assertion = isnull(@assertion, sub.PR_VAL_Assertion)
        ORDER BY
            sub.PR_VAL_ChangedAt DESC,
            sub.PR_VAL_PositedAt DESC,
            sub.PR_VAL_Reliability DESC
    );
GO
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lPR_Pricing viewed by the latest available information for all positors (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[lPR_PricingCommodity]
AS
SELECT
    p.*, 
    cast(null as decimal(5,2)) as Reliability,
    [PR].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tPR_PricingCommodity] (
        p.Positor,
        DEFAULT,
        DEFAULT,
        '+' -- positve assertions only
    ) [PR];
GO
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pPR_Pricing viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[pPR_PricingCommodity] (
    @changingTimepoint datetime2(7)
)
RETURNS TABLE AS RETURN
SELECT
    p.*,
    cast(null as decimal(5,2)) as Reliability,
    [PR].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tPR_PricingCommodity] (
        p.Positor,
        @changingTimepoint,
        DEFAULT,
        '+' -- positve assertions only
    ) [PR];
GO
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nPR_Pricing viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[nPR_PricingCommodity]
AS
SELECT
    p.*, 
    cast(null as decimal(5,2)) as Reliability,
    [PR].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tPR_PricingCommodity] (
        p.Positor,
        sysdatetime(),
        DEFAULT,
        '+' -- positve assertions only
    ) [PR];
GO
-- Difference perspective ---------------------------------------------------------------------------------------------
-- dPR_Pricing showing all differences between the given timepoints and optionally for a subset of attributes
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[dPR_PricingCommodity] (
    @intervalStart datetime2(7),
    @intervalEnd datetime2(7),
    @selection varchar(max) = null
)
RETURNS TABLE AS RETURN
SELECT
    timepoints.inspectedTimepoint,
    [PR].*
FROM
    [dbo].[_Positor] p
JOIN (
    SELECT DISTINCT
        PR_VAL_Positor AS positor,
        PR_VAL_PR_ID AS PR_ID,
        PR_VAL_ChangedAt AS inspectedTimepoint,
        'VAL' AS mnemonic
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index]
    WHERE
        (@selection is null OR @selection like '%VAL%')
    AND
        PR_VAL_ChangedAt BETWEEN @intervalStart AND @intervalEnd
) timepoints
ON
    timepoints.positor = p.Positor
CROSS APPLY
    [dbo].[tPR_PricingCommodity] (
        timepoints.positor,
        timepoints.inspectedTimepoint,
        DEFAULT,
        '+' -- positve assertions only
    ) [PR]
 WHERE
    [PR].PR_ID = timepoints.PR_ID;
GO
-- KEY GENERATORS -----------------------------------------------------------------------------------------------------
--
-- These stored procedures can be used to generate identities of entities.
-- Corresponding anchors must have an incrementing identity column.
--
-- ATTRIBUTE TRIGGERS ------------------------------------------------------------------------------------------------
--
-- The following triggers on the assembled views make them behave like tables.
-- There is one 'instead of' trigger for: insert.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_PR_VAL_PricingCommodity_Index instead of INSERT trigger on PR_VAL_PricingCommodity_Index
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.it_PR_VAL_PricingCommodity_Index', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[it_PR_VAL_PricingCommodity_Index];
GO
CREATE TRIGGER [dbo].[it_PR_VAL_PricingCommodity_Index] ON [dbo].[PR_VAL_PricingCommodity_Index]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;
    DECLARE @PR_VAL_PricingCommodity_Index TABLE (
        PR_VAL_PR_ID int not null,
        PR_VAL_ChangedAt datetime not null,
        PR_VAL_Positor tinyint not null,
        PR_VAL_PositedAt datetime not null,
        PR_VAL_Reliability decimal(5,2) not null,
        PR_VAL_Assertion char(1) not null,
        PR_VAL_PricingCommodity_Index FLOAT not null,
        PR_VAL_Version bigint not null,
        PR_VAL_StatementType char(1) not null,
        primary key(
            PR_VAL_Version,
            PR_VAL_Positor,
            PR_VAL_PR_ID
        )
    );
    INSERT INTO @PR_VAL_PricingCommodity_Index
    SELECT
        i.PR_VAL_PR_ID,
        i.PR_VAL_ChangedAt,
        i.PR_VAL_Positor,
        i.PR_VAL_PositedAt,
        i.PR_VAL_Reliability,
        case
            when i.PR_VAL_Reliability > 0 then '+'
            when i.PR_VAL_Reliability < 0 then '-'
            else '?'
        end,
        i.PR_VAL_PricingCommodity_Index,
        DENSE_RANK() OVER (
            PARTITION BY
                i.PR_VAL_Positor,
                i.PR_VAL_PR_ID
            ORDER BY
                i.PR_VAL_ChangedAt ASC,
                i.PR_VAL_PositedAt ASC,
                i.PR_VAL_Reliability ASC
        ),
        'X'
    FROM
        inserted i;
    SELECT
        @maxVersion = max(PR_VAL_Version),
        @currentVersion = 0
    FROM
        @PR_VAL_PricingCommodity_Index;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.PR_VAL_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.PR_VAL_ID
                        FROM
                            [dbo].[tPR_PricingCommodity](v.PR_VAL_Positor, v.PR_VAL_ChangedAt, v.PR_VAL_PositedAt, v.PR_VAL_Assertion) t
                        WHERE
                            t.PR_VAL_PR_ID = v.PR_VAL_PR_ID
                        AND
                            t.PR_VAL_ChangedAt = v.PR_VAL_ChangedAt
                        AND
                            t.PR_VAL_Reliability = v.PR_VAL_Reliability
                        AND
                            t.PR_VAL_PricingCommodity_Index = v.PR_VAL_PricingCommodity_Index
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.PR_VAL_PR_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.PR_VAL_PricingCommodity_Index
                        WHERE
                            v.PR_VAL_PricingCommodity_Index =
                                dbo.prePR_VAL_PricingCommodity_Index (
                                    v.PR_VAL_PR_ID,
                                    v.PR_VAL_Positor,
                                    v.PR_VAL_ChangedAt,
                                    v.PR_VAL_PositedAt,
                                    v.PR_VAL_Assertion
                                )
                    ) OR EXISTS (
                        SELECT
                            v.PR_VAL_PricingCommodity_Index
                        WHERE
                            v.PR_VAL_PricingCommodity_Index =
                                dbo.folPR_VAL_PricingCommodity_Index (
                                    v.PR_VAL_PR_ID,
                                    v.PR_VAL_Positor,
                                    v.PR_VAL_ChangedAt,
                                    v.PR_VAL_PositedAt,
                                    v.PR_VAL_Assertion
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @PR_VAL_PricingCommodity_Index v
        LEFT JOIN
            [dbo].[PR_VAL_PricingCommodity_Index_Posit] p
        ON
            p.PR_VAL_PR_ID = v.PR_VAL_PR_ID
        AND
            p.PR_VAL_ChangedAt = v.PR_VAL_ChangedAt
        AND
            p.PR_VAL_PricingCommodity_Index = v.PR_VAL_PricingCommodity_Index
        WHERE
            v.PR_VAL_Version = @currentVersion;
        INSERT INTO [dbo].[PR_VAL_PricingCommodity_Index_Posit] (
            PR_VAL_PR_ID,
            PR_VAL_ChangedAt,
            PR_VAL_PricingCommodity_Index
        )
        SELECT
            PR_VAL_PR_ID,
            PR_VAL_ChangedAt,
            PR_VAL_PricingCommodity_Index
        FROM
            @PR_VAL_PricingCommodity_Index
        WHERE
            PR_VAL_Version = @currentVersion
        AND
            PR_VAL_StatementType in ('N','R');
        INSERT INTO [dbo].[PR_VAL_PricingCommodity_Index_Annex] (
            PR_VAL_ID,
            PR_VAL_Positor,
            PR_VAL_PositedAt,
            PR_VAL_Reliability
        )
        SELECT
            p.PR_VAL_ID,
            v.PR_VAL_Positor,
            v.PR_VAL_PositedAt,
            v.PR_VAL_Reliability
        FROM
            @PR_VAL_PricingCommodity_Index v
        JOIN
            [dbo].[PR_VAL_PricingCommodity_Index_Posit] p
        ON
            p.PR_VAL_PR_ID = v.PR_VAL_PR_ID
        AND
            p.PR_VAL_ChangedAt = v.PR_VAL_ChangedAt
        AND
            p.PR_VAL_PricingCommodity_Index = v.PR_VAL_PricingCommodity_Index
        WHERE
            v.PR_VAL_Version = @currentVersion
        AND
            PR_VAL_StatementType in ('S','N','R');
    END
END
GO
-- ANCHOR TRIGGERS ---------------------------------------------------------------------------------------------------
--
-- The following triggers on the latest view make it behave like a table.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_lPR_Pricing instead of INSERT trigger on lPR_Pricing
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[it_lPR_PricingCommodity] ON [dbo].[lPR_PricingCommodity]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    DECLARE @PR TABLE (
        Row bigint IDENTITY(1,1) not null primary key,
        PR_ID int not null
    );
    INSERT INTO [dbo].[PR_PricingCommodity] (
        PR_Dummy
    )
    OUTPUT
        inserted.PR_ID
    INTO
        @PR
    SELECT
        null
    FROM
        inserted
    WHERE
        inserted.PR_ID is null;
    DECLARE @inserted TABLE (
        PR_ID int not null,
        PR_VAL_PR_ID int null,
        PR_VAL_ChangedAt datetime null,
        PR_VAL_Positor tinyint null,
        PR_VAL_PositedAt datetime null,
        PR_VAL_Reliability decimal(5,2) null,
        PR_VAL_PricingCommodity_Index FLOAT null
    );
    INSERT INTO @inserted
    SELECT
        ISNULL(i.PR_ID, a.PR_ID),
        ISNULL(ISNULL(i.PR_VAL_PR_ID, i.PR_ID), a.PR_ID),
        ISNULL(i.PR_VAL_ChangedAt, @now),
        ISNULL(ISNULL(i.PR_VAL_Positor, i.Positor), 0),
        ISNULL(i.PR_VAL_PositedAt, @now),
        ISNULL(ISNULL(i.PR_VAL_Reliability, i.Reliability), 0),
        i.PR_VAL_PricingCommodity_Index
    FROM (
        SELECT
            Positor,
            Reliability,
            PR_ID,
            PR_VAL_PR_ID,
            PR_VAL_ChangedAt,
            PR_VAL_Positor,
            PR_VAL_PositedAt,
            PR_VAL_Reliability,
            PR_VAL_PricingCommodity_Index,
            ROW_NUMBER() OVER (PARTITION BY PR_ID ORDER BY PR_ID) AS Row
        FROM
            inserted
    ) i
    LEFT JOIN
        @PR a
    ON
        a.Row = i.Row;
    INSERT INTO [dbo].[PR_VAL_PricingCommodity_Index] (
        PR_VAL_PR_ID,
        PR_VAL_PricingCommodity_Index,
        PR_VAL_ChangedAt,
        PR_VAL_PositedAt,
        PR_VAL_Positor,
        PR_VAL_Reliability
    )
    SELECT
        i.PR_VAL_PR_ID,
        i.PR_VAL_PricingCommodity_Index,
        i.PR_VAL_ChangedAt,
        i.PR_VAL_PositedAt,
        i.PR_VAL_Positor,
        i.PR_VAL_Reliability
    FROM
        @inserted i
    WHERE
        i.PR_VAL_PricingCommodity_Index is not null;
END
GO
-- UPDATE trigger -----------------------------------------------------------------------------------------------------
-- ut_lPR_Pricing instead of UPDATE trigger on lPR_Pricing
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[ut_lPR_PricingCommodity] ON [dbo].[lPR_PricingCommodity]
INSTEAD OF UPDATE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    IF(UPDATE(PR_ID))
        RAISERROR('The identity column PR_ID is not updatable.', 16, 1);
    IF(UPDATE(PR_VAL_ID))
        RAISERROR('The identity column PR_VAL_ID is not updatable.', 16, 1);
    IF(UPDATE(PR_VAL_PR_ID))
        RAISERROR('The foreign key column PR_VAL_PR_ID is not updatable.', 16, 1);
    IF(UPDATE(PR_VAL_Assertion))
        RAISERROR('The computed assertion column PR_VAL_Assertion is not updatable.', 16, 1);
    IF (
        UPDATE(PR_VAL_PricingCommodity_Index) OR
        UPDATE(PR_VAL_Reliability) OR
        UPDATE(Reliability)
    )
    BEGIN
        INSERT INTO [dbo].[PR_VAL_PricingCommodity_Index] (
            PR_VAL_PR_ID,
            PR_VAL_PricingCommodity_Index,
            PR_VAL_ChangedAt,
            PR_VAL_PositedAt,
            PR_VAL_Positor,
            PR_VAL_Reliability
        )
        SELECT
            ISNULL(i.PR_VAL_PR_ID, i.PR_ID),
            i.PR_VAL_PricingCommodity_Index,
            cast(CASE
                WHEN UPDATE(Reliability) AND NOT UPDATE(PR_VAL_ChangedAt) THEN i.PR_VAL_ChangedAt
                WHEN UPDATE(PR_VAL_ChangedAt) THEN i.PR_VAL_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE 
                WHEN UPDATE(PR_VAL_PositedAt) THEN i.PR_VAL_PositedAt 
                ELSE @now 
            END as datetime),
            CASE 
                WHEN UPDATE(Positor) THEN i.Positor 
                ELSE ISNULL(i.PR_VAL_Positor, 0) 
            END,
            CASE
                WHEN UPDATE(PR_VAL_Reliability) THEN ISNULL(i.PR_VAL_Reliability, 0)
                WHEN UPDATE(Reliability) THEN Reliability
                ELSE ISNULL(i.PR_VAL_Reliability, 0)
            END
        FROM
            inserted i
        WHERE
            i.PR_VAL_PricingCommodity_Index is not null 
    END
END
GO
-- DELETE trigger -----------------------------------------------------------------------------------------------------
-- dt_lPR_Pricing instead of DELETE trigger on lPR_Pricing
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[dt_lPR_PricingCommodity] ON [dbo].[lPR_PricingCommodity]
INSTEAD OF DELETE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    INSERT INTO [dbo].[PR_VAL_PricingCommodity_Index_Annex] (
        PR_VAL_ID,
        PR_VAL_Positor,
        PR_VAL_PositedAt,
        PR_VAL_Reliability
    )
    SELECT
        p.PR_VAL_ID,
        p.PR_VAL_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[PR_VAL_PricingCommodity_Index_Annex] p
    ON
        p.PR_VAL_ID = d.PR_VAL_ID;
END
GO
-- TIES ---------------------------------------------------------------------------------------------------------------
--
-- Ties are used to represent relationships between entities.
-- They come in four flavors: static, historized, knotted static, and knotted historized.
-- Ties have cardinality, constraining how members may participate in the relationship.
-- Every entity that is a member in a tie has a specified role in the relationship.
-- Ties must have at least two anchor roles and zero or more knot roles.
--
-- TIE ASSEMBLED VIEWS ------------------------------------------------------------------------------------------------
--
-- The assembled view of a tie combines the posit and annex table of the tie.
-- It can be used to maintain entity integrity through a primary key, which cannot be
-- defined elsewhere.
--
-- TIE REWINDERS AND FORWARDERS ---------------------------------------------------------------------------------------
--
-- These table valued functions rewind a tie posit table to the given
-- point in changing time, or a tie annex table to the given point
-- in positing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time. The forwarder is the opposite of the rewinder, such that 
-- their union corresponds to all rows in the posit table.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to rewind to (defaults to End of Time, no rewind)
-- @positingTimepoint the point in positing time to rewind to (defaults to End of Time, no rewind)
--
-- TIE TEMPORAL PERSPECTIVES ------------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each tie. There are four types of perspectives: latest,
-- point-in-time, difference, and now.
--
-- The time traveling perspective shows information as it was or will be based on a number
-- of input parameters.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to travel to (defaults to End of Time)
-- @positingTimepoint the point in positing time to travel to (defaults to End of Time)
-- @assertion whether to show positve, negative, uncertain, or all posits (defaults to all)
--
-- The latest perspective shows the latest available information for each tie.
-- The now perspective shows the information as it is right now.
-- The point-in-time perspective lets you travel through the information to the given timepoint.
--
-- @changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints.
--
-- @intervalStart the start of the interval for finding changes
-- @intervalEnd the end of the interval for finding changes
--
-- TIE TRIGGERS -------------------------------------------------------------------------------------------------------
--
-- The following triggers on the assembled and latest views make them behave like tables.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent ties, only changes that represent values different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Attribute sheet stack ----------------------------------------------------------------------------------------------
-- ssPR_VAL_PricingCommodity_Index procedure returning one temporal sheet per positor
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.ssPR_VAL_PricingCommodity_Index', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[ssPR_VAL_PricingCommodity_Index];
GO
CREATE PROCEDURE [dbo].[ssPR_VAL_PricingCommodity_Index] (
    @PR_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), PR_VAL_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[PR_VAL_PricingCommodity_Index]
        WHERE
            PR_VAL_PR_ID = ISNULL(@PR_ID, PR_VAL_PR_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        PR_VAL_Positor
    FROM
        [dbo].[PR_VAL_PricingCommodity_Index]
    WHERE
        PR_VAL_PR_ID = ISNULL(@PR_ID, PR_VAL_PR_ID)
    AND
        PR_VAL_PricingCommodity_Index is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.PR_VAL_Positor,
                x.PR_VAL_PR_ID,
                x.PR_VAL_PositedAt,
                b.PR_VAL_Assertion,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    PR_VAL_PR_ID,
                    PR_VAL_PositedAt
                FROM
                    [dbo].[PR_VAL_PricingCommodity_Index]
                WHERE
                    PR_VAL_PR_ID = ISNULL(@PR_ID, PR_VAL_PR_ID)
            ) x
            LEFT JOIN (
                SELECT
                    PR_VAL_Positor,
                    PR_VAL_PR_ID,
                    PR_VAL_PositedAt,
                    PR_VAL_Assertion,
                    ' + @pivot + '
                FROM
                    [dbo].[PR_VAL_PricingCommodity_Index]
                PIVOT (
                    MIN(PR_VAL_PricingCommodity_Index) FOR PR_VAL_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    PR_VAL_PR_ID = ISNULL(@PR_ID, PR_VAL_PR_ID)
                AND
                    PR_VAL_Positor = ' + @positor + '
            ) b
            ON
                b.PR_VAL_PR_ID = x.PR_VAL_PR_ID
            AND
                b.PR_VAL_PositedAt = x.PR_VAL_PositedAt
            ORDER BY
                PR_VAL_PR_ID,
                PR_VAL_PositedAt,
                PR_VAL_Assertion
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END
GO
-- SCHEMA EVOLUTION ---------------------------------------------------------------------------------------------------
--
-- The following tables, views, and functions are used to track schema changes
-- over time, as well as providing every XML that has been 'executed' against
-- the database.
--
-- Schema table -------------------------------------------------------------------------------------------------------
-- The schema table holds every xml that has been executed against the database
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Schema', 'U') IS NULL
   CREATE TABLE [dbo].[_Schema] (
      [version] int identity(1, 1) not null,
      [activation] datetime2(7) not null,
      [schema] xml not null,
      constraint pk_Schema primary key (
         [version]
      )
   );
GO
-- Insert the XML schema (as of now)
INSERT INTO [dbo].[_Schema] (
   [activation],
   [schema]
)
SELECT
   current_timestamp,
   N'<schema format="0.99" date="2015-12-07" time="09:31:21"><metadata changingRange="datetime" encapsulation="dbo" identity="int" metadataPrefix="Metadata" metadataType="int" metadataUsage="false" changingSuffix="ChangedAt" identitySuffix="ID" positIdentity="int" positGenerator="true" positingRange="datetime" positingSuffix="PositedAt" positorRange="tinyint" positorSuffix="Positor" reliabilityRange="decimal(5,2)" reliabilitySuffix="Reliability" deleteReliability="0" assertionSuffix="Assertion" partitioning="false" entityIntegrity="false" restatability="true" idempotency="false" assertiveness="true" naming="improved" positSuffix="Posit" annexSuffix="Annex" chronon="datetime2(7)" now="sysdatetime()" dummySuffix="Dummy" versionSuffix="Version" statementTypeSuffix="StatementType" checksumSuffix="Checksum" businessViews="false" decisiveness="false" equivalence="false" equivalentSuffix="EQ" equivalentRange="tinyint" databaseTarget="SQLServer" temporalization="crt"/><anchor mnemonic="PR" descriptor="Pricing" identity="int"><metadata capsule="dbo" generator="false"/><attribute mnemonic="VAL" descriptor="Index" identity="int" timeRange="datetime" dataRange="FLOAT"><metadata capsule="dbo" generator="false" assertive="false" restatable="true" idempotent="false"/><layout x="1012.65" y="583.27" fixed="false"/></attribute><layout x="960.00" y="554.50" fixed="false"/></anchor></schema>';
GO
-- Schema expanded view -----------------------------------------------------------------------------------------------
-- A view of the schema table that expands the XML attributes into columns
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Schema_Expanded', 'V') IS NOT NULL
DROP VIEW [dbo].[_Schema_Expanded]
GO
CREATE VIEW [dbo].[_Schema_Expanded]
AS
SELECT
	[version],
	[activation],
	[schema],
	[schema].value('schema[1]/@format', 'nvarchar(max)') as [format],
	[schema].value('schema[1]/@date', 'datetime') + [schema].value('schema[1]/@time', 'datetime') as [date],
	[schema].value('schema[1]/metadata[1]/@temporalization', 'nvarchar(max)') as [temporalization],
	[schema].value('schema[1]/metadata[1]/@databaseTarget', 'nvarchar(max)') as [databaseTarget],
	[schema].value('schema[1]/metadata[1]/@changingRange', 'nvarchar(max)') as [changingRange],
	[schema].value('schema[1]/metadata[1]/@encapsulation', 'nvarchar(max)') as [encapsulation],
	[schema].value('schema[1]/metadata[1]/@identity', 'nvarchar(max)') as [identity],
	[schema].value('schema[1]/metadata[1]/@metadataPrefix', 'nvarchar(max)') as [metadataPrefix],
	[schema].value('schema[1]/metadata[1]/@metadataType', 'nvarchar(max)') as [metadataType],
	[schema].value('schema[1]/metadata[1]/@metadataUsage', 'nvarchar(max)') as [metadataUsage],
	[schema].value('schema[1]/metadata[1]/@changingSuffix', 'nvarchar(max)') as [changingSuffix],
	[schema].value('schema[1]/metadata[1]/@identitySuffix', 'nvarchar(max)') as [identitySuffix],
	[schema].value('schema[1]/metadata[1]/@positIdentity', 'nvarchar(max)') as [positIdentity],
	[schema].value('schema[1]/metadata[1]/@positGenerator', 'nvarchar(max)') as [positGenerator],
	[schema].value('schema[1]/metadata[1]/@positingRange', 'nvarchar(max)') as [positingRange],
	[schema].value('schema[1]/metadata[1]/@positingSuffix', 'nvarchar(max)') as [positingSuffix],
	[schema].value('schema[1]/metadata[1]/@positorRange', 'nvarchar(max)') as [positorRange],
	[schema].value('schema[1]/metadata[1]/@positorSuffix', 'nvarchar(max)') as [positorSuffix],
	[schema].value('schema[1]/metadata[1]/@reliabilityRange', 'nvarchar(max)') as [reliabilityRange],
	[schema].value('schema[1]/metadata[1]/@reliabilitySuffix', 'nvarchar(max)') as [reliabilitySuffix],
	[schema].value('schema[1]/metadata[1]/@deleteReliability', 'nvarchar(max)') as [deleteReliability],
	[schema].value('schema[1]/metadata[1]/@assertionSuffix', 'nvarchar(max)') as [assertionSuffix],
	[schema].value('schema[1]/metadata[1]/@partitioning', 'nvarchar(max)') as [partitioning],
	[schema].value('schema[1]/metadata[1]/@entityIntegrity', 'nvarchar(max)') as [entityIntegrity],
	[schema].value('schema[1]/metadata[1]/@restatability', 'nvarchar(max)') as [restatability],
	[schema].value('schema[1]/metadata[1]/@idempotency', 'nvarchar(max)') as [idempotency],
	[schema].value('schema[1]/metadata[1]/@assertiveness', 'nvarchar(max)') as [assertiveness],
	[schema].value('schema[1]/metadata[1]/@naming', 'nvarchar(max)') as [naming],
	[schema].value('schema[1]/metadata[1]/@positSuffix', 'nvarchar(max)') as [positSuffix],
	[schema].value('schema[1]/metadata[1]/@annexSuffix', 'nvarchar(max)') as [annexSuffix],
	[schema].value('schema[1]/metadata[1]/@chronon', 'nvarchar(max)') as [chronon],
	[schema].value('schema[1]/metadata[1]/@now', 'nvarchar(max)') as [now],
	[schema].value('schema[1]/metadata[1]/@dummySuffix', 'nvarchar(max)') as [dummySuffix],
	[schema].value('schema[1]/metadata[1]/@statementTypeSuffix', 'nvarchar(max)') as [statementTypeSuffix],
	[schema].value('schema[1]/metadata[1]/@checksumSuffix', 'nvarchar(max)') as [checksumSuffix],
	[schema].value('schema[1]/metadata[1]/@businessViews', 'nvarchar(max)') as [businessViews],
	[schema].value('schema[1]/metadata[1]/@equivalence', 'nvarchar(max)') as [equivalence],
	[schema].value('schema[1]/metadata[1]/@equivalentSuffix', 'nvarchar(max)') as [equivalentSuffix],
	[schema].value('schema[1]/metadata[1]/@equivalentRange', 'nvarchar(max)') as [equivalentRange]
FROM
	_Schema;
GO
-- Anchor view --------------------------------------------------------------------------------------------------------
-- The anchor view shows information about all the anchors in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Anchor', 'V') IS NOT NULL
DROP VIEW [dbo].[_Anchor]
GO
CREATE VIEW [dbo].[_Anchor]
AS
SELECT
   S.version,
   S.activation,
   Nodeset.anchor.value('concat(@mnemonic, "_", @descriptor)', 'nvarchar(max)') as [name],
   Nodeset.anchor.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.anchor.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.anchor.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.anchor.value('@identity', 'nvarchar(max)') as [identity],
   Nodeset.anchor.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.anchor.value('count(attribute)', 'int') as [numberOfAttributes]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/anchor') as Nodeset(anchor);
GO
-- Knot view ----------------------------------------------------------------------------------------------------------
-- The knot view shows information about all the knots in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Knot', 'V') IS NOT NULL
DROP VIEW [dbo].[_Knot]
GO
CREATE VIEW [dbo].[_Knot]
AS
SELECT
   S.version,
   S.activation,
   Nodeset.knot.value('concat(@mnemonic, "_", @descriptor)', 'nvarchar(max)') as [name],
   Nodeset.knot.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.knot.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.knot.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.knot.value('@identity', 'nvarchar(max)') as [identity],
   Nodeset.knot.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.knot.value('@dataRange', 'nvarchar(max)') as [dataRange],
   isnull(Nodeset.knot.value('metadata[1]/@checksum', 'nvarchar(max)'), 'false') as [checksum],
   isnull(Nodeset.knot.value('metadata[1]/@equivalent', 'nvarchar(max)'), 'false') as [equivalent]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/knot') as Nodeset(knot);
GO
-- Attribute view -----------------------------------------------------------------------------------------------------
-- The attribute view shows information about all the attributes in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Attribute', 'V') IS NOT NULL
DROP VIEW [dbo].[_Attribute]
GO
CREATE VIEW [dbo].[_Attribute]
AS
SELECT
   S.version,
   S.activation,
   ParentNodeset.anchor.value('concat(@mnemonic, "_")', 'nvarchar(max)') +
   Nodeset.attribute.value('concat(@mnemonic, "_")', 'nvarchar(max)') +
   ParentNodeset.anchor.value('concat(@descriptor, "_")', 'nvarchar(max)') +
   Nodeset.attribute.value('@descriptor', 'nvarchar(max)') as [name],
   Nodeset.attribute.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.attribute.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.attribute.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.attribute.value('@identity', 'nvarchar(max)') as [identity],
   isnull(Nodeset.attribute.value('metadata[1]/@equivalent', 'nvarchar(max)'), 'false') as [equivalent],
   Nodeset.attribute.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.attribute.value('metadata[1]/@assertive', 'nvarchar(max)') as [assertive],
   isnull(Nodeset.attribute.value('metadata[1]/@checksum', 'nvarchar(max)'), 'false') as [checksum],
   Nodeset.attribute.value('metadata[1]/@restatable', 'nvarchar(max)') as [restatable],
   Nodeset.attribute.value('metadata[1]/@idempotent', 'nvarchar(max)') as [idempotent],
   ParentNodeset.anchor.value('@mnemonic', 'nvarchar(max)') as [anchorMnemonic],
   ParentNodeset.anchor.value('@descriptor', 'nvarchar(max)') as [anchorDescriptor],
   ParentNodeset.anchor.value('@identity', 'nvarchar(max)') as [anchorIdentity],
   Nodeset.attribute.value('@dataRange', 'nvarchar(max)') as [dataRange],
   Nodeset.attribute.value('@knotRange', 'nvarchar(max)') as [knotRange],
   Nodeset.attribute.value('@timeRange', 'nvarchar(max)') as [timeRange]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/anchor') as ParentNodeset(anchor)
OUTER APPLY
   ParentNodeset.anchor.nodes('attribute') as Nodeset(attribute);
GO
-- Tie view -----------------------------------------------------------------------------------------------------------
-- The tie view shows information about all the ties in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Tie', 'V') IS NOT NULL
DROP VIEW [dbo].[_Tie]
GO
CREATE VIEW [dbo].[_Tie]
AS
SELECT
   S.version,
   S.activation,
   REPLACE(Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"]
      return concat($role/@type, "_", $role/@role)
   ').value('.', 'nvarchar(max)'), ' ', '_') as [name],
   Nodeset.tie.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.tie.value('count(anchorRole) + count(knotRole)', 'int') as [numberOfRoles],
   Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"]
      return string($role/@role)
   ').value('.', 'nvarchar(max)') as [roles],
   Nodeset.tie.value('count(anchorRole)', 'int') as [numberOfAnchors],
   Nodeset.tie.query('
      for $role in anchorRole
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [anchors],
   Nodeset.tie.value('count(knotRole)', 'int') as [numberOfKnots],
   Nodeset.tie.query('
      for $role in knotRole
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [knots],
   Nodeset.tie.value('count(*[local-name() = "anchorRole" or local-name() = "knotRole"][@identifier = "true"])', 'int') as [numberOfIdentifiers],
   Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"][@identifier = "true"]
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [identifiers],
   Nodeset.tie.value('@timeRange', 'nvarchar(max)') as [timeRange],
   Nodeset.tie.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.tie.value('metadata[1]/@assertive', 'nvarchar(max)') as [assertive],
   Nodeset.tie.value('metadata[1]/@restatable', 'nvarchar(max)') as [restatable],
   Nodeset.tie.value('metadata[1]/@idempotent', 'nvarchar(max)') as [idempotent]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/tie') as Nodeset(tie);
GO
-- Evolution function -------------------------------------------------------------------------------------------------
-- The evolution function shows what the schema looked like at the given
-- point in time with additional information about missing or added
-- modeling components since that time.
--
-- @timepoint The point in time to which you would like to travel.
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Evolution', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[_Evolution];
GO
CREATE FUNCTION [dbo].[_Evolution] (
    @timepoint AS datetime2(7)
)
RETURNS TABLE AS
RETURN
WITH constructs AS (
   SELECT
      temporalization,
      [capsule] + '.' + [name] + s.suffix AS [qualifiedName],
      [version],
      [activation]
   FROM 
      [dbo].[_Anchor] a
   CROSS APPLY (
      VALUES ('uni', ''), ('crt', '')
   ) s (temporalization, suffix)
   UNION ALL
   SELECT
      temporalization,
      [capsule] + '.' + [name] + s.suffix AS [qualifiedName],
      [version],
      [activation]
   FROM
      [dbo].[_Knot] k
   CROSS APPLY (
      VALUES ('uni', ''), ('crt', '')
   ) s (temporalization, suffix)
   UNION ALL
   SELECT
      temporalization,
      [capsule] + '.' + [name] + s.suffix AS [qualifiedName],
      [version],
      [activation]
   FROM
      [dbo].[_Attribute] b
   CROSS APPLY (
      VALUES ('uni', ''), ('crt', '_Annex'), ('crt', '_Posit')
   ) s (temporalization, suffix)
   UNION ALL
   SELECT
      temporalization,
      [capsule] + '.' + [name] + s.suffix AS [qualifiedName],
      [version],
      [activation]
   FROM
      [dbo].[_Tie] t
   CROSS APPLY (
      VALUES ('uni', ''), ('crt', '_Annex'), ('crt', '_Posit')
   ) s (temporalization, suffix)
), 
selectedSchema AS (
   SELECT TOP 1
      *
   FROM
      [dbo].[_Schema_Expanded]
   WHERE
      [activation] <= @timepoint
   ORDER BY
      [activation] DESC
),
presentConstructs AS (
   SELECT
      C.*
   FROM
      selectedSchema S
   JOIN
      constructs C
   ON
      S.[version] = C.[version]
   AND
      S.temporalization = C.temporalization 
), 
allConstructs AS (
   SELECT
      C.*
   FROM
      selectedSchema S
   JOIN
      constructs C
   ON
      S.temporalization = C.temporalization
)
SELECT
   COALESCE(P.[version], X.[version]) as [version],
   COALESCE(P.[qualifiedName], T.[qualifiedName]) AS [name],
   COALESCE(P.[activation], X.[activation], T.[create_date]) AS [activation],
   CASE
      WHEN P.[activation] = S.[activation] THEN 'Present'
      WHEN X.[activation] > S.[activation] THEN 'Future'
      WHEN X.[activation] < S.[activation] THEN 'Past'
      ELSE 'Missing'
   END AS Existence
FROM 
   presentConstructs P
FULL OUTER JOIN (
   SELECT 
      s.[name] + '.' + t.[name] AS [qualifiedName],
      t.[create_date]
   FROM 
      sys.tables t
   JOIN
      sys.schemas s
   ON
      s.schema_id = t.schema_id
   WHERE
      t.[type] = 'U'
   AND
      LEFT(t.[name], 1) <> '_'
) T
ON
   T.[qualifiedName] = P.[qualifiedName]
LEFT JOIN
   allConstructs X
ON
   X.[qualifiedName] = T.[qualifiedName]
AND
   X.[activation] = (
      SELECT
         MIN(sub.[activation])
      FROM
         constructs sub
      WHERE
         sub.[qualifiedName] = T.[qualifiedName]
      AND 
         sub.[activation] >= T.[create_date]
   )
CROSS APPLY (
   SELECT
      *
   FROM
      selectedSchema
) S;
GO
-- Drop Script Generator ----------------------------------------------------------------------------------------------
-- generates a drop script, that must be run separately, dropping everything in an Anchor Modeled database
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._GenerateDropScript', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_GenerateDropScript];
GO
CREATE PROCEDURE [dbo]._GenerateDropScript (
   @exclusionPattern varchar(42) = '%.[[][_]%', -- exclude Metadata by default
   @inclusionPattern varchar(42) = '%', -- include everything by default
   @directions varchar(42) = 'Upwards, Downwards', -- do both up and down by default
   @qualifiedName varchar(555) = null -- can specify a single object
)
AS
BEGIN
   set nocount on;
   select
      ordinal,
      unqualifiedName,
      qualifiedName
   into 
      #constructs
   from (
      select distinct
         10 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + ']' as qualifiedName
      from
         [dbo]._Attribute
      union all
      select distinct
         11 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + '_Annex]' as qualifiedName
      from
         [dbo]._Attribute
      union all
      select distinct
         12 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + '_Posit]' as qualifiedName
      from
         [dbo]._Attribute
      union all
      select distinct
         20 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + ']' as qualifiedName
      from
         [dbo]._Tie
      union all
      select distinct
         21 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + '_Annex]' as qualifiedName
      from
         [dbo]._Tie
      union all
      select distinct
         22 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + '_Posit]' as qualifiedName
      from
         [dbo]._Tie
      union all
      select distinct
         30 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + ']' as qualifiedName
      from
         [dbo]._Knot
      union all
      select distinct
         40 as ordinal,
         name as unqualifiedName,
         '[' + capsule + '].[' + name + ']' as qualifiedName
      from
         [dbo]._Anchor
   ) t;
   select
      c.ordinal,
      cast(c.unqualifiedName as nvarchar(517)) as unqualifiedName,
      cast(c.qualifiedName as nvarchar(517)) as qualifiedName,
      o.[object_id],
      o.[type]
   into
      #includedConstructs
   from
      #constructs c
   join
      sys.objects o
   on
      o.[object_id] = OBJECT_ID(c.qualifiedName)
   and 
      o.[type] = 'U'
   where
      OBJECT_ID(c.qualifiedName) = OBJECT_ID(isnull(@qualifiedName, c.qualifiedName));
   create unique clustered index ix_includedConstructs on #includedConstructs([object_id]);
   with relatedUpwards as (
      select
         c.[object_id],
         c.[type],
         c.unqualifiedName,
         c.qualifiedName,
         1 as depth
      from
         #includedConstructs c
      union all
      select
         o.[object_id],
         o.[type],
         n.unqualifiedName,
         n.qualifiedName,
         c.depth + 1 as depth
      from
         relatedUpwards c
      cross apply
         sys.dm_sql_referencing_entities(c.qualifiedName, 'OBJECT') r
      cross apply (
         select
            cast('[' + r.referencing_schema_name + '].[' + r.referencing_entity_name + ']' as nvarchar(517)),
            cast(r.referencing_entity_name as nvarchar(517))
      ) n (qualifiedName, unqualifiedName)
      join
         sys.objects o
      on
         o.[object_id] = r.referencing_id
      and
         o.type not in ('S')
      where 
         r.referencing_id <> OBJECT_ID(c.qualifiedName)
   )
   select distinct
      [object_id],
      [type],
      unqualifiedName,
      qualifiedName,
      depth
   into
      #relatedUpwards
   from
      relatedUpwards u
   where
      depth = (
         select
            MAX(depth)
         from
            relatedUpwards s
         where
            s.[object_id] = u.[object_id]
      );
   create unique clustered index ix_relatedUpwards on #relatedUpwards([object_id]);
   with relatedDownwards as (
      select
         cast('Upwards' as varchar(42)) as [relationType],
         c.[object_id],
         c.[type],
         c.unqualifiedName, 
         c.qualifiedName,
         c.depth
      from
         #relatedUpwards c 
      union all
      select
         cast('Downwards' as varchar(42)) as [relationType],
         o.[object_id],
         o.[type],
         n.unqualifiedName, 
         n.qualifiedName,
         c.depth - 1 as depth
      from
         relatedDownwards c
      cross apply
         sys.dm_sql_referenced_entities(c.qualifiedName, 'OBJECT') r
      cross apply (
         select
            cast('[' + r.referenced_schema_name + '].[' + r.referenced_entity_name + ']' as nvarchar(517)),
            cast(r.referenced_entity_name as nvarchar(517))
      ) n (qualifiedName, unqualifiedName)
      join
         sys.objects o
      on
         o.[object_id] = r.referenced_id
      and
         o.type not in ('S')
      where
         r.referenced_minor_id = 0
      and 
         r.referenced_id <> OBJECT_ID(c.qualifiedName)
      and
         r.referenced_id not in (select [object_id] from #relatedUpwards)
   )
   select distinct
      relationType,
      [object_id],
      [type],
      unqualifiedName,
      qualifiedName,
      depth
   into
      #relatedDownwards
   from
      relatedDownwards d
   where
      depth = (
         select
            MIN(depth)
         from
            relatedDownwards s
         where
            s.[object_id] = d.[object_id]
      );
   create unique clustered index ix_relatedDownwards on #relatedDownwards([object_id]);
   select distinct
      [object_id],
      [type],
      [unqualifiedName],
      [qualifiedName],
      [depth]
   into
      #affectedObjects
   from
      #relatedDownwards d
   where
      [qualifiedName] not like @exclusionPattern
   and
      [qualifiedName] like @inclusionPattern
   and
      @directions like '%' + [relationType] + '%';
   create unique clustered index ix_affectedObjects on #affectedObjects([object_id]);
   select distinct
      objectType,
      unqualifiedName,
      qualifiedName,
      dropOrder
   into
      #dropList
   from (
      select
         t.objectType,
         o.unqualifiedName,
         o.qualifiedName,
         dense_rank() over (
            order by
               o.depth desc,
               case o.[type]
                  when 'C' then 0 -- CHECK CONSTRAINT
                  when 'TR' then 1 -- SQL_TRIGGER
                  when 'P' then 2 -- SQL_STORED_PROCEDURE
                  when 'V' then 3 -- VIEW
                  when 'IF' then 4 -- SQL_INLINE_TABLE_VALUED_FUNCTION
                  when 'FN' then 5 -- SQL_SCALAR_FUNCTION
                  when 'PK' then 6 -- PRIMARY_KEY_CONSTRAINT
                  when 'UQ' then 7 -- UNIQUE_CONSTRAINT
                  when 'F' then 8 -- FOREIGN_KEY_CONSTRAINT
                  when 'U' then 9 -- USER_TABLE
               end asc,
               isnull(c.ordinal, 0) asc
         ) as dropOrder
      from
         #affectedObjects o
      left join
         #includedConstructs c
      on
         c.[object_id] = o.[object_id]
      cross apply (
         select
            case o.[type]
               when 'C' then 'CHECK'
               when 'TR' then 'TRIGGER'
               when 'V' then 'VIEW'
               when 'IF' then 'FUNCTION'
               when 'FN' then 'FUNCTION'
               when 'P' then 'PROCEDURE'
               when 'PK' then 'CONSTRAINT'
               when 'UQ' then 'CONSTRAINT'
               when 'F' then 'CONSTRAINT'
               when 'U' then 'TABLE'
            end
         ) t (objectType)
      where
         t.objectType in (
            'CHECK',
            'VIEW',
            'FUNCTION',
            'PROCEDURE',
            'TABLE'
         )
   ) r;
   select
      case 
         when d.objectType = 'CHECK'
         then 'ALTER TABLE ' + p.parentName + ' DROP CONSTRAINT ' + d.unqualifiedName
         else 'DROP ' + d.objectType + ' ' + d.qualifiedName
      end + ';' + CHAR(13) as [text()]
   from
      #dropList d
   join
      sys.objects o
   on
      o.[object_id] = OBJECT_ID(d.qualifiedName)
   join
      sys.schemas s
   on
      s.[schema_id] = o.[schema_id]
   cross apply (
      select
         '[' + s.name + '].[' + OBJECT_NAME(o.parent_object_id) + ']'
   ) p (parentName)
   order by
      d.dropOrder asc
   for xml path('');
END
GO
-- Database Copy Script Generator -------------------------------------------------------------------------------------
-- generates a copy script, that must be run separately, copying all data between two identically modeled databases
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._GenerateCopyScript', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_GenerateCopyScript];
GO
CREATE PROCEDURE [dbo]._GenerateCopyScript (
	@source varchar(123),
	@target varchar(123)
)
as
begin
	declare @R char(1);
    set @R = CHAR(13);
	-- stores the built SQL code
	declare @sql varchar(max);
    set @sql = 'USE ' + @target + ';' + @R;
	declare @xml xml;
	-- find which version of the schema that is in effect
	declare @version int;
	select
		@version = max([version])
	from
		_Schema;
	-- declare and set other variables we need
	declare @equivalentSuffix varchar(42);
	declare @identitySuffix varchar(42);
	declare @annexSuffix varchar(42);
	declare @positSuffix varchar(42);
	declare @temporalization varchar(42);
	select
		@equivalentSuffix = equivalentSuffix,
		@identitySuffix = identitySuffix,
		@annexSuffix = annexSuffix,
		@positSuffix = positSuffix,
		@temporalization = temporalization
	from
		_Schema_Expanded
	where
		[version] = @version;
	-- build non-equivalent knot copy
	set @xml = (
		select
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' ON;' + @R
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' OFF;' + @R
			end
		from
			_Knot x
		cross apply (
			select stuff((
				select
					', ' + [name]
				from
					sys.columns
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		and
			isnull(equivalent, 'false') = 'false'
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build equivalent knot copy
	set @xml = (
		select
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @identitySuffix + ' ON;' + @R
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @identitySuffix + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @identitySuffix + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @identitySuffix + ' OFF;' + @R
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @equivalentSuffix + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @equivalentSuffix + ';' + @R
		from
			_Knot x
		cross apply (
			select stuff((
				select
					', ' + [name]
				from
					sys.columns
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		and
			isnull(equivalent, 'false') = 'true'
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build anchor copy
	set @xml = (
		select
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' ON;' + @R
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' OFF;' + @R
			end
		from
			_Anchor x
		cross apply (
			select stuff((
				select
					', ' + [name]
				from
					sys.columns
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build attribute copy
	if (@temporalization = 'crt')
	begin
		set @xml = (
			select
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' ON;' + @R
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @positSuffix + '(' + [positColumns] + ')' + @R +
				'SELECT ' + [positColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @positSuffix + ';' + @R,
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' OFF;' + @R
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @annexSuffix + '(' + [annexColumns] + ')' + @R +
				'SELECT ' + [annexColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @annexSuffix + ';' + @R
			from
				_Attribute x
			cross apply (
				select stuff((
					select
						', ' + [name]
					from
						sys.columns
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @positSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) pc ([positColumns])
			cross apply (
				select stuff((
					select
						', ' + [name]
					from
						sys.columns
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @annexSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) ac ([annexColumns])
			where
				[version] = @version
			for xml path('')
		);
	end
	else -- uni
	begin
		set @xml = (
			select
				'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
				'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R
			from
				_Attribute x
			cross apply (
				select stuff((
					select
						', ' + [name]
					from
						sys.columns
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) c ([columns])
			where
				[version] = @version
			for xml path('')
		);
	end
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build tie copy
	if (@temporalization = 'crt')
	begin
		set @xml = (
			select
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' ON;' + @R
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @positSuffix + '(' + [positColumns] + ')' + @R +
				'SELECT ' + [positColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @positSuffix + ';' + @R,
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' OFF;' + @R
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @annexSuffix + '(' + [annexColumns] + ')' + @R +
				'SELECT ' + [annexColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @annexSuffix + ';' + @R
			from
				_Tie x
			cross apply (
				select stuff((
					select
						', ' + [name]
					from
						sys.columns
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @positSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) pc ([positColumns])
			cross apply (
				select stuff((
					select
						', ' + [name]
					from
						sys.columns
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @annexSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) ac ([annexColumns])
			where
				[version] = @version
			for xml path('')
		);
	end
	else -- uni
	begin
		set @xml = (
			select
				'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
				'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R
			from
				_Tie x
			cross apply (
				select stuff((
					select
						', ' + [name]
					from
						sys.columns
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) c ([columns])
			where
				[version] = @version
			for xml path('')
		);
	end
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	select @sql for xml path('');
end
go
-- Delete Everything with a Certain Metadata Id -----------------------------------------------------------------------
-- deletes all rows from all tables that have the specified metadata id
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._DeleteWhereMetadataEquals', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_DeleteWhereMetadataEquals];
GO
CREATE PROCEDURE [dbo]._DeleteWhereMetadataEquals (
	@metadataID int,
	@schemaVersion int = null,
	@includeKnots bit = 0
)
as
begin
	declare @sql varchar(max);
	set @sql = 'print ''Null is not a valid value for @metadataId''';
	if(@metadataId is not null)
	begin
		if(@schemaVersion is null)
		begin
			select
				@schemaVersion = max(Version)
			from
				_Schema;
		end;
		with constructs as (
			select
				'l' + name as name,
				2 as prio,
				'Metadata_' + name as metadataColumn
			from
				_Tie
			where
				[version] = @schemaVersion
			union all
			select
				'l' + name as name,
				3 as prio,
				'Metadata_' + mnemonic as metadataColumn
			from
				_Anchor
			where
				[version] = @schemaVersion
			union all
			select
				name,
				4 as prio,
				'Metadata_' + mnemonic as metadataColumn
			from
				_Knot
			where
				[version] = @schemaVersion
			and
				@includeKnots = 1
		)
		select
			@sql = (
				select
					'DELETE FROM ' + name + ' WHERE ' + metadataColumn + ' = ' + cast(@metadataId as varchar(10)) + '; '
				from
					constructs
        order by
					prio, name
				for xml
					path('')
			);
	end
	exec(@sql);
end
go
-- DESCRIPTIONS -------------------------------------------------------------------------------------------------------