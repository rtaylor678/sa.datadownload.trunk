﻿CREATE FUNCTION [etl].[AR_Unit_toCurrency]
(
	@DisplayName	VARCHAR(128),
	@Unit			VARCHAR(16)
)
RETURNS VARCHAR(7)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@i		INT	= CHARINDEX('/', @Unit);
	DECLARE	@Tag	VARCHAR(7) = CASE WHEN @i >= 1 THEN LEFT(@Unit, @i - 1) END;

	SET	@Tag = CASE WHEN @Unit = 'USD' THEN @Unit ELSE @Tag END;

	SET	@Tag = CASE @Tag
				WHEN 'bl'		THEN 'UNS'
				WHEN 'p'		THEN 'UNS'
				WHEN 'No Unit'	THEN 'UNS'
				WHEN 'Euro'		THEN 'EUR'
				WHEN 'Dinar'	THEN CASE WHEN CHARINDEX('Bahrain', @DisplayName) >= 1 THEN 'BHD' END
				ELSE @Tag
				END;

	SET @Tag	= COALESCE(@Tag, 'UNS');

	RETURN @Tag;

END;
GO