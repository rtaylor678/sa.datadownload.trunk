﻿CREATE TABLE [dim].[LO_Location_Bridge]
(
	[LO_MethodologyId]		INT					NOT	NULL,

	[LO_LocationId]			INT					NOT NULL	CONSTRAINT [FK__LO_Location_Bridge_LO_LocationId]					REFERENCES [dim].[LO_Location_LookUp]([LO_LocationId])
															CONSTRAINT [FK__LO_Location_Bridge_LO_Location_Parent_Ancestor]		FOREIGN KEY ([LO_MethodologyId], [LO_LocationId])
																																REFERENCES [dim].[LO_Location_Parent]([LO_MethodologyId], [LO_LocationId]),

	[LO_DescendantId]		INT					NOT NULL	CONSTRAINT [FK__LO_Location_Bridge_LO_DescendantId]					REFERENCES [dim].[LO_Location_LookUp]([LO_LocationId])
															CONSTRAINT [FK__LO_Location_Bridge_LO_Location_Parent_Descendant]	FOREIGN KEY ([LO_MethodologyId], [LO_LocationId])
																																REFERENCES [dim].[LO_Location_Parent]([LO_MethodologyId], [LO_LocationId]),
	
	[LO_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__LO_Location_Bridge_LO_PositedBy]					DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__LO_Location_Bridge_LO_PositedBy]					REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[LO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__LO_Location_Bridge_LO_PositedAt]					DEFAULT(SYSDATETIMEOFFSET()),
	[LO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__LO_Location_Bridge_LO_RowGuid]						DEFAULT(newsequentialid())	ROWGUIDCOL,
															CONSTRAINT [UX__LO_Location_Bridge_LO_RowGuid]						UNIQUE NONCLUSTERED([LO_RowGuid]),

	CONSTRAINT [PK__LO_Location_Bridge]	PRIMARY KEY CLUSTERED([LO_MethodologyId] ASC, [LO_LocationId] ASC, [LO_DescendantId] ASC)
);