﻿PRINT 'insert.etl.Argus...';

DECLARE @Etl_UomArgus TABLE
(
	[MP_SourceTag]				VARCHAR(9)			NOT	NULL	CHECK([MP_SourceTag] <> ''),
	[MP_TargetId]				INT					NOT	NULL,
	[MP_Multiplier]				FLOAT				NOT NULL	CHECK([MP_Multiplier] >= 0.0)
	UNIQUE CLUSTERED([MP_SourceTag] ASC)
);

INSERT INTO @Etl_UomArgus([MP_SourceTag], [MP_TargetId], [MP_Multiplier])
SELECT [t].[MP_SourceTag], [l].[UM_UomId], [t].[MP_Multiplier]
FROM (VALUES
	('''000 lots',	'Lots',   0.001),
	('''000 m3',	'KM3',	   1.0),
	('''000t',		'TON',	1000.0),
	('100l',		'L',	 100.0),
	('allowance',	'ALW',	   1.0),
	('bl',			'B',	   1.0),
	('bl/day',		'B/D',	   1.0),
	('bushel',		'BSL',	   1.0),
	('day',			'D',	   1.0),
	('days',		'D',	   1.0),
	('dmtu',		'MT',	   1.0),
	('Flask',		'FL',	   1.0),
	('GWh',			'GWH',	   1.0),

	('litre',		'L',	   1.0),
	('long ton',	'LT',	   1.0),
	('lots',		'Lots',	   1.0),

	('mn lb',		'MLB',	   1.0),
	('mnBtu',		'MBTU',	   1.0),
	('mtu',			'MT',	   1.0),
	('No Unit',		'UNS',	   1.0),
	('p/th',		'THR',	   1.0),
	('RIN',			'RNC',	   1.0),
	('t',			'TON',	   1.0),
	('t MGOe',		'tMGOe',   1.0),

	('USD',			'UNS',	   1.0),
	('USG',			'GAL',	   1.0),
	('WS',			'WS',	   1.0),

	('N/A',			'N/A',	   1.0),
	('UNS',			'UNS',	   1.0)
	)[t]([MP_SourceTag], [MP_TargetTag], [MP_Multiplier])
INNER JOIN
	[dim].[UM_Uom_LookUp]	[l]
		ON	[l].[UM_Tag]	= [t].[MP_TargetTag];

DECLARE @Etl_CurArgus TABLE
(
	[MP_SourceTag]				VARCHAR(7)			NOT	NULL	CHECK([MP_SourceTag] <> ''),
	[MP_TargetId]				INT					NOT	NULL,
	[MP_Multiplier]				FLOAT				NOT NULL	CHECK([MP_Multiplier] >= 0.0)
	UNIQUE CLUSTERED([MP_SourceTag] ASC)
);

INSERT INTO @Etl_CurArgus([MP_SourceTag], [MP_TargetId], [MP_Multiplier])
SELECT [t].[MP_SourceTag], [l].[CU_CurrencyId], [t].[MP_Multiplier]
FROM (VALUES
	('USC',		'USD',	   0.01),
	('CAC',		'CAD',	   0.01)
	)[t]([MP_SourceTag], [MP_TargetTag], [MP_Multiplier])
INNER JOIN
	[dim].[CU_Currency_LookUp]	[l]
		ON	[l].[CU_Tag]	= [t].[MP_TargetTag];

INSERT INTO [etl].[MP_Argus_Posit]
(
	[MP_Code],
	[MP_FrequencyId],
	[MP_UomId],
	[MP_UomMultiplier],
	[MP_CurrencyId],
	[MP_CurrencyMultiplier]
)
SELECT
	[r].[AR_Code],

		[f].[FQ_FrequencyId],

		[MP_UomId]				= COALESCE([l].[UM_UomId], [u].[MP_TargetId]),
		[MP_UomMultiplier]		= COALESCE([u].[MP_Multiplier], 1.0),

		[MP_CurrencyId]			= COALESCE([k].[CU_CurrencyId], [c].[MP_TargetId]),
		[MP_CurrencyMultiplier]	= COALESCE([c].[MP_Multiplier], 1.0)
FROM
	[ref].[AR_Codes_Posit]				[r]
INNER JOIN
	[dim].[FQ_Frequency_LookUp]			[f]
		ON	[f].[FQ_Name]				= [r].[AR_Frequency]
LEFT OUTER JOIN
	@Etl_UomArgus						[u]
		ON	[u].[MP_SourceTag]			= [etl].[AR_Unit_toUom]([r].[AR_DisplayName], [r].[AR_Unit])
LEFT OUTER JOIN
	[dim].[UM_Uom_LookUp]				[l]
		ON	[l].[UM_Tag]				= [etl].[AR_Unit_toUom]([r].[AR_DisplayName], [r].[AR_Unit])
LEFT OUTER JOIN
	@Etl_CurArgus						[c]
		ON	[c].[MP_SourceTag]			= [etl].[AR_Unit_toCurrency]([r].[AR_DisplayName], [r].[AR_Unit])
LEFT OUTER JOIN
	[dim].[CU_Currency_LookUp]			[k]
		ON	[k].[CU_Tag]				= [etl].[AR_Unit_toCurrency]([r].[AR_DisplayName], [r].[AR_Unit])
LEFT OUTER JOIN
	[etl].[MP_Argus_Posit]				[m]
		ON	[m].[MP_Code]				= [r].[AR_Code]
WHERE
	[m].[MP_MapId]		IS NULL;
