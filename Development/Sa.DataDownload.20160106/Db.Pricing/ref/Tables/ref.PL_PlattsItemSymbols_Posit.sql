﻿CREATE TABLE [ref].[PL_PlattsItemSymbols_Posit]
(
	[PL_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[PL_Category]				VARCHAR(3)			NOT	NULL,	CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Category]			CHECK([PL_Category]		<> ''),
	[PL_Symbol]					CHAR(7)				NOT	NULL,	CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Symbol]			CHECK([PL_Symbol]		<> ''),
	[PL_Bate]					CHAR(1)				NOT	NULL,	CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Bate]				CHECK([PL_Bate]			<> ''),

	[PL_SymbolBate]				AS CONVERT(CHAR(8), [PL_Symbol] + [PL_Bate])
								PERSISTED			NOT	NULL,

	[PL_Frequency]				CHAR(2)				NOT	NULL,	CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Freq]				CHECK([PL_Frequency]	<> ''),
	[PL_Currency]				CHAR(3)				NOT	NULL,	CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Curr]				CHECK([PL_Currency]		<> ''),
	[PL_Uom]					VARCHAR(3)			NOT	NULL,	CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Uom]				CHECK([PL_Uom]			<> ''),
	
	[PL_Earliest]				DATE					NULL,
	[PL_Latest]					DATE					NULL,	CONSTRAINT [CR__PL_PlattsItemSymbols_Posit_PL_DateRange]		CHECK([PL_Earliest]		<= [PL_Latest]),
	
	[PL_Detail]					VARCHAR(192)		NOT	NULL,	CONSTRAINT [CR__PL_PlattsItemSymbols_Posit_PL_Detail]			CHECK([PL_Detail]		<> ''),

	[PL_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[PL_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PL_PlattsItemSymbols_Posit_PL_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PL_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[PL_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_Reliability]		DEFAULT(50),
	[PL_PositReliable]			AS CONVERT(BIT, CASE WHEN [PL_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PL_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PP_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PL_PlattsItemSymbols_Posit_PP_RowGuid]			UNIQUE NONCLUSTERED([PL_RowGuid]),

	CONSTRAINT [PK__PL_PlattsItemSymbols_Posit] PRIMARY KEY([PL_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PL_PlattsItemSymbols_Posit] UNIQUE CLUSTERED([PL_Symbol] ASC, [PL_Bate] ASC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE NONCLUSTERED INDEX [IX__PL_PlattsItemSymbols_Posit_PL_Uom]
ON [ref].[PL_PlattsItemSymbols_Posit]([PL_Uom])
INCLUDE ([PL_Symbol],[PL_Currency])
GO

CREATE NONCLUSTERED INDEX [IX__PL_PlattsItemSymbols_Posit_PL_Currency]
ON [ref].[PL_PlattsItemSymbols_Posit]([PL_Currency])
INCLUDE ([PL_Symbol],[PL_Uom])
GO