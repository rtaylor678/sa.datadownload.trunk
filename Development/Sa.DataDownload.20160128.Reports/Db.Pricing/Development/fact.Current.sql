﻿--SELECT [p].[PP_ItemSymbol], [Items] = COUNT(1) FROM [stage].[PP_PricingPlattsFtp_Posit_Pivot] [p] 
--WHERE [p].[PP_ItemSymbol] IN (SELECT [x].[PP_ItemSymbol] FROM [stage].[PP_PricingPlattsFtp_Posit_Pivot] [x] WHERE [x].[PP_ItemXAction_Stamp] IS NOT NULL AND [x].[PP_Index] IS NOT NULL)
--GROUP BY [p].[PP_ItemSymbol] ORDER BY COUNT(1) DESC;

DECLARE	@ChangedAfter	DATETIMEOFFSET(7)	= '2015-02-05 00:00:00.0000000 -00:00';
DECLARE	@ChangedBefore	DATETIMEOFFSET(7)	= '2015-02-12 00:00:00.0000000 -00:00';
DECLARE	@PositedBefore	DATETIMEOFFSET(7)	= '2015-12-04 20:00:00.0000000 -06:00';
DECLARE	@ItemSymbol		VARCHAR(7)			= 'AAJND00';
DECLARE @MaterialId		INT					= (SELECT TOP 1 [m].[MT_MaterialId] FROM [dim].[MT_Material_LookUp] [m] WHERE [m].[MT_Tag] = @ItemSymbol);



--[Pricing [156]] Error: SSIS Error Code DTS_E_OLEDBERROR.  An OLE DB error has occurred. Error code: 0x80040E2F.
--An OLE DB record is available.  Source: "Microsoft SQL Server Native Client 11.0"  Hresult: 0x80040E2F  Description: "The statement has been terminated.".
--An OLE DB record is available.  Source: "Microsoft SQL Server Native Client 11.0"  Hresult: 0x80040E2F  Description: "Violation of PRIMARY KEY constraint 'PK__PR_VAL_PricingCommodity_Value_Annex'. Cannot insert duplicate key in object 'fact.PR_VAL_PricingCommodity_Value_Annex'.
-- The duplicate key value is (2436, 2015-12-07 11:10:50.0000000 -06:00, 2).".

SELECT * FROM [fact].[PR_VAL_PricingCommodity_Value_Posit] [p] WHERE [p].[PR_VAL_ID] = 2436;
SELECT * FROM [fact].[PR_PricingCommodity] [p] WHERE [p].[PR_ID] = 2435;
SELECT * FROM [dim].[MT_Material_LookUp] [l] WHERE [l].[MT_MaterialId] = 8152;
SELECT * FROM [stage].[PP_PricingPlattsFtp_Posit_Pivot] [p] WHERE [p].[PP_ItemSymbol] = 'AAWBA00';



SELECT DISTINCT [p].[PP_ItemTrans] FROM [stage].[PP_PricingPlattsFtp_Posit_Pivot] [p] WHERE [p].[PP_ItemTrans] <> 'X'

