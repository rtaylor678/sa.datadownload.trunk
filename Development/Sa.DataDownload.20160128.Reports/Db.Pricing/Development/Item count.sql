﻿SELECT
	[k].[CO_Tag],
	[k].[CO_Detail],
	[d].[DS_Detail],
		[Ann]	= YEAR([c].[PR_PriceDate]),
		[Mth]	= MONTH([c].[PR_PriceDate]),

	[Items PR_VAL_Open]				= COUNT([c].[PR_VAL_Open]),
	[Mean PR_VAL_Open]				= AVG([c].[PR_VAL_Open]),

	[Items PR_VAL_Close]			= COUNT([c].[PR_VAL_Close]),
	[Mean PR_VAL_Close]				= AVG([c].[PR_VAL_Close]),

	[Items PR_VAL_Low]				= COUNT([c].[PR_VAL_Low]),
	[Mean PR_VAL_Low]				= AVG([c].[PR_VAL_Low]),
	
	[Items PR_VAL_High]				= COUNT([c].[PR_VAL_High]),
	[Mean PR_VAL_High]				= AVG([c].[PR_VAL_High]),
	
	[Items PR_VAL_Volume]			= COUNT([c].[PR_VAL_Volume]),
	[Mean PR_VAL_Volume]			= AVG([c].[PR_VAL_Volume]),
	
	[Items PR_TRD_Index]			= COUNT([c].[PR_TRD_Index]),
	[Mean PR_TRD_Index]				= AVG([c].[PR_TRD_Index]),
	
	[Items PR_TRD_Mean]				= COUNT([c].[PR_TRD_Mean]),
	[Mean PR_TRD_Mean]				= AVG([c].[PR_TRD_Mean]),
	
	[Items PR_TRD_Ask]				= COUNT([c].[PR_TRD_Ask]),
	[Mean PR_TRD_Ask]				= AVG([c].[PR_TRD_Ask]),
	
	[Items PR_TRD_Bid]				= COUNT([c].[PR_TRD_Bid]),
	[Mean PR_TRD_Bid]				= AVG([c].[PR_TRD_Bid]),
	
	[Items PR_TRD_PrevInterest]		= COUNT([c].[PR_TRD_PrevInterest]),
	[Mean PR_TRD_PrevInterest]		= AVG([c].[PR_TRD_PrevInterest]),
	
	[Items PR_TRD_Rate]				= COUNT([c].[PR_TRD_Rate]),
	[Mean PR_TRD_Rate]				= AVG([c].[PR_TRD_Rate]),
	
	[Items PR_TRD_Margin]			= COUNT([c].[PR_TRD_Margin]),
	[Mean PR_TRD_Margin]			= AVG([c].[PR_TRD_Margin]),
	
	[Items PR_TRD_DiffLow]			= COUNT([c].[PR_TRD_DiffLow]),
	[Mean PR_TRD_DiffLow]			= AVG([c].[PR_TRD_DiffLow]),
	
	[Items PR_TRD_DiffHigh]			= COUNT([c].[PR_TRD_DiffHigh]),
	[Mean PR_TRD_DiffHigh]			= AVG([c].[PR_TRD_DiffHigh]),
	
	[Items PR_TRD_DiffIndex]		= COUNT([c].[PR_TRD_DiffIndex]),
	[Mean PR_TRD_DiffIndex]			= AVG([c].[PR_TRD_DiffIndex]),

	[Items PR_TRD_DiffMidpoint]		= COUNT([c].[PR_TRD_DiffMidpoint]),
	[Mean PR_TRD_DiffMidpoint]		= AVG([c].[PR_TRD_DiffMidpoint]),

	[Items PR_TRD_Midpoint]			= COUNT([c].[PR_TRD_Midpoint]),
	[Mean PR_TRD_Midpoint]			= AVG([c].[PR_TRD_Midpoint]),
	
	[Items PR_TRD_Netback]			= COUNT([c].[PR_TRD_Netback]),
	[Mean PR_TRD_Netback]			= AVG([c].[PR_TRD_Netback]),
	
	[Items PR_TRD_NetbackMargin]	= COUNT([c].[PR_TRD_NetbackMargin]),
	[Mean PR_TRD_NetbackMargin]		= AVG([c].[PR_TRD_NetbackMargin]),
	
	[Items PR_TRD_RGV]				= COUNT([c].[PR_TRD_RGV]),
	[Mean PR_TRD_RGV]				= AVG([c].[PR_TRD_RGV]),
	
	[Items PR_TRD_CumulativeIndex]	= COUNT([c].[PR_TRD_CumulativeIndex]),
	[Mean PR_TRD_CumulativeIndex]	= AVG([c].[PR_TRD_CumulativeIndex]),
	
	[Items PR_TRD_CumulativeVolume]	= COUNT([c].[PR_TRD_CumulativeVolume]),
	[Mean PR_TRD_CumulativeVolume]	= AVG([c].[PR_TRD_CumulativeVolume]),
	
	[Items PR_TRD_TransportCosts]	= COUNT([c].[PR_TRD_TransportCosts]),
	[Mean PR_TRD_TransportCosts]	= AVG([c].[PR_TRD_TransportCosts])

FROM
	[fact].[PR_PricingCommodity_Current]	[c]
INNER JOIN
	[dim].[CO_Commodity_LookUp]				[k]
		ON	[k].[CO_CommodityId]			= [c].[PR_CommodityId]
INNER JOIN
	[dim].[DS_DataSource_LookUp]			[d]
		ON	[d].[DS_DataSourceId]			= [c].[PR_DataSourceId]
GROUP BY
	[k].[CO_Tag],
	[k].[CO_Detail],
	[d].[DS_Detail],
		YEAR([c].[PR_PriceDate]),
		MONTH([c].[PR_PriceDate])
ORDER BY
	[k].[CO_Tag],
	[k].[CO_Detail],
	[d].[DS_Detail],
		YEAR([c].[PR_PriceDate]),
		MONTH([c].[PR_PriceDate]);
