﻿CREATE TABLE [ref].[AR_Modules_Posit]
(
	[AR_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[AR_Module]					VARCHAR(16)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AR_Module]			CHECK([AR_Module] <> ''),
	[AR_Path]					VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AR_Path]			CHECK([AR_Path] <> ''),
	[AR_FileName]				VARCHAR(16)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AR_FileName]		CHECK([AR_FileName] <> ''),
	[AR_Description]			VARCHAR(64)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AR_Description]	CHECK([AR_Description] <> ''),
	[AR_Folder]					VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AR_Folder]			CHECK([AR_Folder] <> ''),
	[AR_Time]					CHAR(12)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AR_Time]			CHECK([AR_Time] <> ''),
	[AR_LocalTime]				TIME				NOT	NULL,
	[AR_LocalTimeZone]			VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__AR_Modules_Posit_AM_LocalTimeZone]	CHECK([AR_LocalTimeZone] <> ''),


	[AR_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_Modules_Posit_AR_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__AR_Modules_Posit_AR_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AR_Modules_Posit_AR_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AR_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_Modules_Posit_AR_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__AR_Modules_Posit_AR_Reliability]	DEFAULT(50),
	[AR_PositReliable]			AS CONVERT(BIT, CASE WHEN [AR_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[AR_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AR_Modules_Posit_AR_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__AR_Modules_Posit_AR_RowGuid]		UNIQUE NONCLUSTERED([AR_RowGuid]),

	CONSTRAINT [PK__AR_Modules_Posit] PRIMARY KEY([AR_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__AR_Modules_Posit] UNIQUE CLUSTERED([AR_Module] ASC)
		WITH(FILLFACTOR = 95)
);