﻿SET NOCOUNT ON;

:r .\..\Security\Proxies\PxySsisPricing.sql

:r .\Post\posit\insert.posit.Positor.sql
:r .\Post\dim\insert.dim.Operator.sql

:r .\Seed\ArgusCsv\insert.Argus.reference.sql
:r .\Seed\PlattsFtp\insert.Platts.reference.sym.sql

:r .\Post\dim\insert.dim.Currency.sql
:r .\Post\dim\insert.dim.DataSource.sql
:r .\Post\dim\insert.dim.Frequency.sql
:r .\Post\dim\insert.dim.Location.sql
:r .\Post\dim\insert.dim.Uom.sql

:r .\Post\dim\insert.dim.Commodity.sql

:r .\Post\etl\insert.etl.Argus.sql
:r .\Post\etl\insert.etl.Platts.sql

--:r .\Seed\ArgusCsv\insert.Argus.seed.sql
--:r .\Seed\PlattsFtp\insert.Platts.seed.sql
