﻿CREATE USER [SOLOMONWORLD\SsisPricing]
	FOR LOGIN [SOLOMONWORLD\SsisPricing]
	WITH DEFAULT_SCHEMA=[dbo];
GO

EXECUTE sp_addrolemember 'db_datareader', 'SOLOMONWORLD\SsisPricing';
GO

EXECUTE sp_addrolemember 'db_datawriter', 'SOLOMONWORLD\SsisPricing';
GO

GRANT CONNECT TO [SOLOMONWORLD\SsisPricing];
GO

GRANT EXECUTE TO [SOLOMONWORLD\SsisPricing];
GO