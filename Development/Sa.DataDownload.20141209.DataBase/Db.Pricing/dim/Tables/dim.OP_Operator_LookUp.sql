﻿CREATE TABLE [dim].[OP_Operator_LookUp]
(
	[OP_OperatorId]			CHAR(1)				NOT	NULL	CONSTRAINT [CL__OP_Operator_LookUp_OP_OperatorId]		CHECK([OP_OperatorId] <> ''),

	[OP_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__OP_Operator_LookUp_OP_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__OP_Operator_LookUp_OP_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[OP_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__OP_Operator_LookUp_OP_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[OP_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__OP_Operator_LookUp_OP_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__OP_Operator_LookUp_OP_RowGuid]			UNIQUE NONCLUSTERED([OP_RowGuid]),

	CONSTRAINT [PK__OP_Operator_LookUp]			PRIMARY KEY CLUSTERED([OP_OperatorId])
);