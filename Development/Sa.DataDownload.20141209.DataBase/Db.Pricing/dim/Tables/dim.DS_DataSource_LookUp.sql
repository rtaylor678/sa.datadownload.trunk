﻿CREATE TABLE [dim].[DS_DataSource_LookUp]
(
	[DS_DataSourceId]		INT					NOT NULL	IDENTITY(1, 1),

	[DS_Tag]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__DS_DataSource_LookUp_DS_Tag]			CHECK([DS_Tag] <> ''),
															CONSTRAINT [UK__DS_DataSource_LookUp_DS_Tag]			UNIQUE CLUSTERED([DS_Tag] ASC),

	[DS_Name]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__DS_DataSource_LookUp_DS_Name]			CHECK([DS_Name] <> '')
															CONSTRAINT [UX__DS_DataSource_LookUp_DS_Name]			UNIQUE([DS_Name] ASC),

	[DS_Detail]				NVARCHAR(96)		NOT	NULL	CONSTRAINT [CL__DS_DataSource_LookUp_DS_Detail]			CHECK([DS_Detail] <> '')
															CONSTRAINT [UX__DS_DataSource_LookUp_DS_Detail]			UNIQUE([DS_Detail] ASC),

	[DS_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__DS_DataSource_LookUp_DS_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__DS_DataSource_LookUp_DS_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[DS_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__DS_DataSource_LookUp_DS_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[DS_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__DS_DataSource_LookUp_DS_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__DS_DataSource_LookUp_DS_RowGuid]		UNIQUE NONCLUSTERED([DS_RowGuid]),

	CONSTRAINT [PK__DS_DataSource_LookUp]		PRIMARY KEY([DS_DataSourceId] ASC)
);