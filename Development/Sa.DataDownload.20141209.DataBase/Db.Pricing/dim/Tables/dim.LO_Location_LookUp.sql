﻿CREATE TABLE [dim].[LO_Location_LookUp]
(
	[LO_LocationId]			INT					NOT NULL	IDENTITY(1, 1),

	[LO_Tag]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__LO_Location_LookUp_LO_Tag]				CHECK([LO_Tag] <> ''),
															CONSTRAINT [UK__LO_Location_LookUp_LO_Tag]				UNIQUE CLUSTERED([LO_Tag] ASC),

	[LO_Name]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__LO_Location_LookUp_LO_Name]				CHECK([LO_Name] <> '')
															CONSTRAINT [UX__LO_Location_LookUp_LO_Name]				UNIQUE([LO_Name] ASC),

	[LO_Detail]				NVARCHAR(192)		NOT	NULL	CONSTRAINT [CL__LO_Location_LookUp_LO_Detail]			CHECK([LO_Detail] <> '')
															CONSTRAINT [UX__LO_Location_LookUp_LO_Detail]			UNIQUE([LO_Detail] ASC),

	[LO_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__LO_Location_LookUp_LO_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__LO_Location_LookUp_LO_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[LO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__LO_Location_LookUp_LO_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[LO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__LO_Location_LookUp_LO_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__LO_Location_LookUp_LO_RowGuid]			UNIQUE NONCLUSTERED([LO_RowGuid]),

	CONSTRAINT [PK__LO_Location_LookUp]			PRIMARY KEY([LO_LocationId] ASC)
);