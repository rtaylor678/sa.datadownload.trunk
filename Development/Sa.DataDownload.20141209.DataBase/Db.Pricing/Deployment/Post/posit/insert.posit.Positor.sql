﻿PRINT 'insert.posit.Positor...';

DECLARE @ProcedureDesc	NVARCHAR(257) = OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID);
DECLARE @PositorNote	NVARCHAR(348) = N'Automatically created by: ' + @ProcedureDesc;

EXECUTE [posit].[Insert_Positor] @PositorNote;