﻿PRINT 'insert.etl.Platts...';

DECLARE @Etl_UomPlattsMarketData TABLE
(
	[MP_SourceTag]				VARCHAR(3)			NOT	NULL	CHECK([MP_SourceTag] <> ''),
	[MP_TargetId]				INT					NOT	NULL,
	[MP_Multiplier]				FLOAT				NOT NULL	CHECK([MP_Multiplier] >= 0.0)
	UNIQUE CLUSTERED([MP_SourceTag] ASC)
);

INSERT INTO @Etl_UomPlattsMarketData([MP_SourceTag], [MP_TargetId], [MP_Multiplier])
SELECT [t].[MP_SourceTag], [l].[UM_UomId], [t].[MP_Multiplier]
FROM (VALUES
	('ALW',	'ALWEMI',	   1.0),	--+	Allowance (Emissions)
	('BBD',	'B/D',		   1.0),	--+	Barrels/Day
	('BBL',	'B',		   1.0),	--	BARREL
	('BCF',	'BCF',		   1.0),	--+	CUBIC FEET (BILLIONS)
	('BKH',	'BTU/HR',	   1.0),	--+	BTU/KILOWATT HOUR
	('BSL',	'BSL',		   1.0),	--+	BUSHEL
	('CM',	'M3',		   1.0),	--	CUBIC METER
	('CMT',	'KM3',		   1.0),	--	CUBIC METERS (THOUSAND)
	('CON',	'CON',		   1.0),	--+	CONTRACTS (NO OF)
	('CWT',	'LB',		   0.01),	--	HUNDREDWEIGHT (100 lb)
	('DMT',	'MT',		   1.0),	--	DRY METRIC TON
	('FL',	'FL',		   1.0),	--+	FLASK
	('GAL',	'GAL',		   1.0),	--	GALLON
	('GIG',	'GJ',		   1.0),	--	GIGAJOULES
	('GRT',	'TON',		   1.0),	--?	GROSS TONS
	
	('IBP',	'IDX',		   1.0),	--+	INDX VS BS PD=100

	('KG',	'KG',		   1.0),	--	KILOGRAM
	('KLT',	'L',		1000.0),	--	KILOLITER
	('KPB',	'KW/BTU',	   1.0),	--+	KILOWATTS/BTU
	('KT',	'TON',		   0.001),	--?	KILOTON
	('LB',	'LB',		   1.0),	--	POUND
	('LGT',	'LT',		   1.0),	--	LONG TON
	('LSM',	'LSM',		   1.0),	--+	LUMPSUM
	('MBB',	'MB',		   1.0),	--+	BARRELS (MILLIONS)
	('MBD',	'MB/D',		   1.0),	--+	Barrels/Day (Millions)
	('MCM',	'MM3',		   1.0),	--+	CUBIC METERS (MILLIONS)
	('MEG',	'MW',		   1.0),	--	MEGAWATT
	('MGW',	'MWH',		   1.0),	--	MEGAWATT HOURS
	('MMB',	'MBTU',		   1.0),	--	BTU (MILLIONS)
	('MT',	'MT',		   1.0),	--	METRIC TON

	('PCT',	'Percent',	   1.0),	--	PERCENTAGE
	('RNC',	'RNC',		   1.0),	--+	RINS CREDIT
	('ST',	'ST',		   1.0),	--	SHORT TON
	('SWU',	'SWU',		   1.0),	--+	SEPARATIVE WORK UNIT
	('TAF',	'TAF',		   1.0),	--+	THOUSAND OF ACRE FEET
	('TBB',	'KB',		   1.0),	--	BARRELS (THOUSANDS)
	('TBD',	'KB/D',		   1.0),	--+	BARRELS/DAY (THOUSANDS)
	('THR',	'THR',		   1.0),	--+	THERM
	('THT',	'TON',		   0.001),	--	TONS (THOUSANDS)
	('TJL',	'TJ',		   1.0),	--	TERAJOULES
	('TMT',	'KMT',		   1.0),	--	METRIC TON (THOUSANDS)
	('TON',	'TON',		   1.0),	--	TON
	('TOZ',	'TOZ',		   1.0),	--	TROY OUNCE

	('DAG',	'DAG',		   1.0),	--+	(Not in Platts UOM List)
	('DMU',	'DMU',		   1.0),	--+	(Not in Platts UOM List)
	('FT',	'FT',		   1.0),	--+	(Not in Platts UOM List)
	('LTR',	'LTR',		   1.0),	--+	(Not in Platts UOM List)
	('NT',	'NT',		   1.0),	--+	(Not in Platts UOM List)
	('ROC',	'ROC',		   1.0),	--+	(Not in Platts UOM List)
	('WMT',	'WMT',		   1.0),	--+	(Not in Platts UOM List)
	('WSC',	'WSC',		   1.0),	--+	(Not in Platts UOM List)

	('N/A',	'N/A',		   1.0),	--+ Not Applicable
	('UNS',	'UNS',		   1.0)		--+	UNSPECIFIED
	)[t]([MP_SourceTag], [MP_TargetTag], [MP_Multiplier])
INNER JOIN
	[dim].[UM_Uom_LookUp]	[l]
		ON	[l].[UM_Tag]	= [t].[MP_TargetTag];

DECLARE @Etl_CurPlattsMarketData TABLE
(
	[MP_SourceTag]				VARCHAR(3)			NOT	NULL	CHECK([MP_SourceTag] <> ''),
	[MP_TargetId]				INT					NOT	NULL,
	[MP_Multiplier]				FLOAT				NOT NULL	CHECK([MP_Multiplier] >= 0.0)
	UNIQUE CLUSTERED([MP_SourceTag] ASC)
);

INSERT INTO @Etl_CurPlattsMarketData([MP_SourceTag], [MP_TargetId], [MP_Multiplier])
SELECT [t].[MP_SourceTag], [l].[CU_CurrencyId], [t].[MP_Multiplier]
FROM (VALUES
	('CAC', 'CAD',	   0.01),	--	CANADIAN CENTS
	('ERC', 'EUR',	   0.01),	--	EURO CENTS

	('GBC', 'GBP',	   0.01),	--	BRITISH PENCE
	('UKS', 'GBP',	   1.00),	--	UK STERLING

	('USC', 'USD',	   0.01),	--	UNITED STATES CENTS
	('UST', 'USD',	1000.00),	--	US DOLLAR (THOUSANDS)

	('TRL', 'TRLM',	   1.0),	--	Turkish Lira (pre-2005)
	('RMB', 'CNY',	   1.0),	--	Yuan Renminbi (Chinese Yuan) (Not in Platts Currency List)
	('USP', 'FJD',	   1.0)		--	Pacific Islands (Fiji Dollar) (Not in Platts Currency List)

	)[t]([MP_SourceTag], [MP_TargetTag], [MP_Multiplier])
INNER JOIN
	[dim].[CU_Currency_LookUp]	[l]
		ON	[l].[CU_Tag]	= [t].[MP_TargetTag];

INSERT INTO [etl].[MP_Platts_Posit]
(
	[MP_ItemSymbol],
	[MP_UomId],
	[MP_UomMultiplier],
	[MP_CurrencyId],
	[MP_CurrencyMultiplier]
)
SELECT DISTINCT
	[r].[PL_Symbol],

		[MP_UomId]				= [u].[MP_TargetId],
		[MP_UomMultiplier]		= [u].[MP_Multiplier],

		[MP_CurrencyId]			= COALESCE([k].[CU_CurrencyId], [c].[MP_TargetId]),
		[MP_CurrencyMultiplier]	= COALESCE([c].[MP_Multiplier], 1.0)
FROM
	[ref].[PL_PlattsItemSymbols_Posit]	[r]
INNER JOIN
	@Etl_UomPlattsMarketData			[u]
		ON	[u].[MP_SourceTag]			= [r].[PL_Uom]
LEFT OUTER JOIN
	[dim].[CU_Currency_LookUp]			[k]
		ON	[k].[CU_Tag]				= [r].[PL_Currency]
LEFT OUTER JOIN
	@Etl_CurPlattsMarketData			[c]
		ON	[c].[MP_SourceTag]			= [r].[PL_Currency]
LEFT OUTER JOIN
	[etl].[MP_Platts_Posit]				[m]
		ON	[m].[MP_ItemSymbol]			= [r].[PL_Symbol]
WHERE
	[m].[MP_MapId]		IS NULL;