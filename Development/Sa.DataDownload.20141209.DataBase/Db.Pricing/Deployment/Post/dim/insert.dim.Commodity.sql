﻿PRINT 'insert.dim.Location (Argus)...';

INSERT INTO [dim].[CO_Commodity_LookUp]
(
	[CO_Tag],
	[CO_Name]
)
SELECT
	[t].[AR_Code],
	[t].[AR_DisplayName]
FROM
	[ref].[AR_Codes_Posit]				[t]
LEFT OUTER JOIN
	[dim].[CO_Commodity_LookUp]			[x]
		ON	[x].[CO_Tag]				= [t].[AR_Code]
WHERE
	[x].[CO_CommodityId]	IS NULL
ORDER BY
	[t].[AR_Code] ASC;

PRINT 'insert.dim.Location (Platts)...';

INSERT INTO [dim].[CO_Commodity_LookUp]
(
	[CO_Tag],
	[CO_Name]
)
SELECT DISTINCT
	[t].[PL_Symbol],
	[t].[PL_Detail]
FROM
	[ref].[PL_PlattsItemSymbols_Posit]	[t]
LEFT OUTER JOIN
	[dim].[CO_Commodity_LookUp]			[x]
		ON	[x].[CO_Tag]				= [t].[PL_Symbol]
WHERE
	[x].[CO_CommodityId]	IS NULL;