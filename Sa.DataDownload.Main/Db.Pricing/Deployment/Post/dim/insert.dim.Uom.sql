﻿PRINT 'insert.dim.Uom...';

DECLARE @Uom TABLE
(
	[UM_Tag]			VARCHAR(12)			NOT	NULL	CHECK([UM_Tag] <> ''),
														UNIQUE CLUSTERED([UM_Tag] ASC),

	[UM_Name]			NVARCHAR(24)		NOT	NULL	CHECK([UM_Name] <> ''),
														UNIQUE([UM_Name] ASC),

	[UM_Detail]			NVARCHAR(24)		NOT	NULL	CHECK([UM_Detail] <> ''),
														UNIQUE([UM_Detail] ASC),

	[UM_Parent]			VARCHAR(12)			NOT	NULL	CHECK([UM_Parent] <> ''),
	[UM_Factor]			FLOAT					NULL	CHECK([UM_Factor] > 0.0)
);

INSERT INTO @Uom([UM_Tag], [UM_Name], [UM_Detail], [UM_Parent], [UM_Factor])
SELECT [t].[UM_Tag], [t].[UM_Name], [t].[UM_Detail], [t].[UM_Parent], [t].[UM_Factor]
FROM (VALUES
	('Area', 'Area', 'Area', 'Area', NULL),
	('DeltaTemp', 'DeltaTemp', 'DeltaTemp', 'DeltaTemp', NULL),
	('Electric', 'Electric', 'Electric', 'Electric', NULL),
	('Energy', 'Energy', 'Energy', 'Energy', NULL),
	('GasVol', 'GasVol', 'GasVol', 'GasVol', NULL),
	('Density', 'Density', 'Density', 'Density', NULL),
	('Length', 'Length', 'Length', 'Length', NULL),
	('LiqVol', 'LiqVol', 'LiqVol', 'LiqVol', NULL),
	('Power', 'Power', 'Power', 'Power', NULL),
	('Pressure', 'Pressure', 'Pressure', 'Pressure', NULL),
	('Temperature', 'Temperature', 'Temperature', 'Temperature', NULL),
	('Time', 'Time', 'Time', 'Time', NULL),
	('Viscosity', 'Viscosity', 'Viscosity', 'Viscosity', NULL),
	('Percent', 'Percent', 'Percent', 'Percent', NULL),
	('PersTime', 'PersTime', 'PersTime', 'PersTime', NULL),
	('VolPercent', 'VolPercent', 'VolPercent', 'VolPercent', NULL),
	('Weight', 'Weight', 'Weight', 'Weight', NULL),
	('WtPercent', 'WtPercent', 'WtPercent', 'WtPercent', NULL),
	('IDX', 'Index', 'Index', 'Index', NULL),

	('%VOL', N'Vol. %', N'Vol. %', 'VolPercent', 1),
	('%WT', N'Wt. %', N'Wt. %', 'WtPercent', 1),
	('100KWH', N'100 KWH', N'100 KWH', 'Electric', 0.01),
	('ACRE', N'Acre', N'Acre', 'Area', 0.0002471),
	('API', N'Gravity, °API', N'Gravity, °API', 'Density', NULL),
	('ATMA', N'Atmosphere Absolute', N'Atmosphere Absolute', 'Pressure', 0.987),
	('ATMG', N'Atmosphere Gauge', N'Atmosphere Gauge', 'Pressure', 0.987),
	('B', N'Barrel', N'Barrel', 'LiqVol', 0.0238095),
	('BARA', N'Bar Absolute', N'Bar Absolute', 'Pressure', 1),
	('BARG', N'Bar Gauge', N'Bar Gauge', 'Pressure', 1),
	('BHP', N'Brake Horsepower', N'Brake Horsepower', 'Power', 1.34102),
	('BTU', N'BTU', N'BTU', 'Energy', 1e+006),
	('CF', N'ft³', N'ft³', 'LiqVol', 0.133681),
	('CM', N'cm', N'cm', 'Length', 100),
	('CM2', N'cm²', N'cm²', 'Area', 10000),
	('CS100C', N'CS @ 100C', N'CS @ 100C', 'Viscosity', NULL),
	('CS122F', N'CS @ 122F', N'CS @ 122F', 'Viscosity', NULL),
	('CS212F', N'CS @ 212F', N'CS @ 212F', 'Viscosity', NULL),
	('CS40C', N'CS @ 40C', N'CS @ 40C', 'Viscosity', NULL),
	('CS50C', N'CS @ 50C', N'CS @ 50C', 'Viscosity', NULL),
	('D', N'Day', N'Day', 'Time', 1),
	('DTC', N'∆C', N'∆C', 'DeltaTemp', 1),
	('DTF', N'∆°F', N'∆°F', 'DeltaTemp', 1.8),
	('DTK', N'∆K', N'∆K', 'DeltaTemp', 1),
	('DTR', N'∆°R', N'∆°R', 'DeltaTemp', 1.8),
	('FT', N'Foot', N'Foot', 'Length', 3.2808),
	('GAL', N'Gallon', N'Gallon', 'LiqVol', 1),
	('GBTU', N'GBTU', N'GBTU', 'Energy', 0.001),
	('GJ', N'Gigajoule', N'Gigajoule', 'Energy', 1.055),
	('GM', N'gm', N'gm', 'Weight', 453597),
	('HA', N'Hectare', N'Hectare', 'Area', 0.0001),
	('HP', N'Horsepower', N'Horsepower', 'Power', 1.34102),
	('HR', N'Hour', N'Hour', 'Time', 24),
	('INCH', N'Inch', N'Inch', 'Length', 39.3696),
	('KB', N'K Barrels', N'Thousand Barrels', 'LiqVol', 2.38095e-005),
	('KBTU', N'KBTU', N'KBTU', 'Energy', 1000),
	('KG', N'kg', N'kg', 'Weight', 453.597),
	('KGAL', N'K Gallons', N'Thousand Gallons', 'LiqVol', 0.001),
	('KGM2A', N'kg/m² Absolute', N'kg/m² Absolute', 'Pressure', 1.0197),
	('KGM2G', N'kg/m² Gauge', N'kg/m² Gauge', 'Pressure', 1.0197),
	('KGM3', N'Density, kg/m³', N'Density, kg/m³', 'Density', NULL),
	('KHP', N'k Horsepower', N'k Horsepower', 'Power', 0.00134102),
	('KLB', N'K Pounds', N'K Pounds', 'Weight', 1),
	('KM', N'km', N'km', 'Length', 0.001),
	('KM3', N'K m³', N'K m³', 'LiqVol', 3.78544e-006),
	('KMT', N'K Metric Tons', N'K Metric Tons', 'Weight', 0.000453593),
	('KNM3', N'K nm³', N'K nm³', 'GasVol', 0.02679),
	('KPASA', N'Kilopascal Absolute', N'Kilopascal Absolute', 'Pressure', 100),
	('KPASG', N'Kilopascal Gauge', N'Kilopascal Gauge', 'Pressure', 100),
	('KSCF', N'KSCF', N'KSCF', 'GasVol', 1),
	('KW', N'KW', N'KW', 'Power', 1),
	('KWH', N'KWH', N'KWH', 'Electric', 1),
	('L', N'Liter', N'Liter', 'LiqVol', 3.78544),
	('LB', N'Pound', N'Pound', 'Weight', 1000),
	('LT', N'Long Ton', N'Long Ton', 'Weight', 0.446429),
	('M', N'Meter', N'Meter', 'Length', 1),
	('M2', N'm²', N'm²', 'Area', 1),
	('M3', N'm³', N'm³', 'LiqVol', 0.00378544),
	('MBTU', N'M BTU', N'Million BTU', 'Energy', 1),
	('MGAL', N'M Gallons', N'M Gallons', 'LiqVol', 1e-006),
	('MILE', N'Mile', N'Mile', 'Length', 0.000621364),
	('MIN', N'Minute', N'Minute', 'Time', 1440),
	('MJ', N'Megajoule', N'Megajoule', 'Energy', 1055),
	('MLB', N'M Pounds', N'M Pounds', 'Weight', 0.001),
	('MM', N'mm', N'mm', 'Length', 1000),
	('MMHGA', N'mm Hg Absolute', N'mm Hg Absolute', 'Pressure', 750.19),
	('MMHGG', N'mm Hg Gauge', N'mm Hg Gauge', 'Pressure', 750.19),
	('MMJ', N'Million MJ', N'Million MJ', 'Energy', 0.001055),
	('MNM3', N'M nm³', N'M nm³', 'GasVol', 2.679e-005),
	('MONTH', N'Month', N'Month', 'Time', 0.0328542),
	('MSCF', N'MSCF', N'MSCF', 'GasVol', 0.001),
	('MT', N'Metric Tons', N'Metric Tons', 'Weight', 0.453593),
	('MW', N'MW', N'MW', 'Power', 0.001),
	('MWH', N'MWH', N'MWH', 'Electric', 0.001),
	('NM3', N'nm³', N'nm³', 'GasVol', 26.79),
	('OZ', N'Ounce', N'Ounce', 'Weight', 16000),
	('PPMV', N'ppm (Vol.)', N'ppm (Vol.)', 'VolPercent', 10000),
	('PPMW', N'ppm (Wt.)', N'ppm (Wt.)', 'WtPercent', 10000),
	('PSIA', N'PSIA', N'PSIA', 'Pressure', 14.504),
	('PSIG', N'PSIG', N'PSIG', 'Pressure', 14.504),
	('SCF', N'SCF', N'SCF', 'GasVol', 1000),
	('SEC', N'Second', N'Second', 'Time', 86400),
	('SG', N'Specific Gravity', N'Specific Gravity', 'Density', NULL),
	('SLUG', N'Slug', N'Slug', 'Weight', 31.0811),
	('SQFT', N'Sq. ft', N'Sq. ft', 'Area', 10.7636),
	('SQIN', N'Sq. inch', N'Sq. inch', 'Area', 1549.97),
	('SSU100F', N'SSU @ 100F', N'SSU @ 100F', 'Viscosity', NULL),
	('SSU210F', N'SSU @ 210F', N'SSU @ 210F', 'Viscosity', NULL),
	('ST', N'Short Ton', N'Short Ton', 'Weight', 0.5),
	('TC', N'C', N'°C', 'Temperature', NULL),
	('TF', N'F', N'°F', 'Temperature', NULL),
	('TJ', N'Terajoule', N'Terajoule', 'Energy', 0.001055),
	('TK', N'K', N'°K', 'Temperature', NULL),
	('TON', N'Ton', N'Ton', 'Weight', 0.5),
	('TONNE', N'Tonne', N'Tonne', 'Weight', 0.453593),
	('TR', N'R', N'°R', 'Temperature', NULL),
	('WK', N'Week', N'Week', 'Time', 0.142857),
	('YD', N'Yard', N'Yard', 'Length', 1.0936),
	('YR', N'Year', N'Year', 'Time', 0.00273785),
	('TBTU', N'TBTU', N'TBTU', 'Energy', 1e-006),
	('CAL', N'Calorie', N'Calorie', 'Energy', 2.52164e+008),
	('KCAL', N'K Calories', N'K Calories', 'Energy', 252164),
	('MCAL', N'M Calories', N'M Calories', 'Energy', 252.164),
	('GCAL', N'G Calories', N'G Calories', 'Energy', 0.252164),
	('TCAL', N'T Calories', N'T Calories', 'Energy', 0.000252164),

	('MMONTH', N'Man-Month', N'Man-Month', 'PersTime', 12),
	('MHR', N'Man-Hour', N'Man-Hour', 'PersTime', 2080),
	('MYR', N'Man-Year', N'Man-Year', 'PersTime', 1),

	------------------------------------------------------------------------------
	--	Platts UOM Additions

	('ALW', N'Allowance', N'Allowance', 'IDX', NULL),
	('Lots', N'Lots', N'Lots', 'IDX', NULL),

	('GWH', N'GWH', N'GWH', 'Electric', 0.000001),
	('tMGOe', N'M Oersted', N'Mega Oersted', 'IDX', NULL),
	('WS', N'Watt Second', N'Watt Second', 'LiqVol', 1.0),

	------------------------------------------------------------------------------
	--	Platts UOM Additions

	('CWT', N'C Lb (Hundredweight)', N'C Lb (Hundredweight)', 'Weight', 0.01),	--	to Pounds (LB) (100 LBs)
	('BCF', N'B ft³', N'Billion ft³', 'GasVol', 0.000001),						--	to Kscufs (KSCF)
	('FL',  N'Flask', N'Flask', 'LiqVol', 12.7 / 128.0),						--	to Gallons (GAL) (12.7 oz)
	('MB',  N'M Barrels', N'Million Barrels', 'LiqVol', 2.38095e-008),			--	to Gallons (GAL)
	('MM3', N'M m³', N'Million m³', 'LiqVol', 3.78544e-009),					--	to Gallons (GAL)
	('TAF', N'K Acre Feet', N'Thousand Acre Feet', 'LiqVol', 1.0 / 325851.43),	--	to Gallons (GAL)
	('THR', N'Therm', N'Therm', 'Energy', 1.0 / 100000.4),						--	to Million British Thermal Units (MBTU) (100,000 British thermal units)
	('TOZ', N'Troy Ounce', N'Troy Ounce', 'Weight',  0.0685714),				--	to Pounds (LB) (0.0685714 LBS)

	('ALWEMI', N'Allowance (Emissions)', N'Allowance (Emissions)', 'ALW', NULL),
	('BSL', N'Bushel', N'Bushel', 'IDX', NULL),
	('CON', N'Contracts (NO OF)', N'Contracts (Number of)', 'IDX', NULL),
	('LSM', N'Lumpsum', N'Lumpsum', 'IDX', NULL),
	('RNC', N'RIN Credit', N'RIN Credit', 'IDX', NULL),
	('SWU', N'Separative Work Unit', N'Separative Work Unit', 'IDX', NULL),

	('LiqVol/Time', N'LiqVol/Time', N'LiqVol/Time', 'LiqVol/Time', NULL),
	('Energy/Time', N'Energy/Time', N'Energy/Time', 'Energy/Time', NULL),
	('Power/Energy', N'Power/Energy', N'Power/Energy', 'Power/Energy', NULL),

	('B/D', N'Barrels/Day', N'Barrels/Day', 'LiqVol/Time', NULL),
	('BTU/HR', N'BTU/Kilowatt Hour', N'BTU/Kilowatt Hour', 'Energy/Time', NULL),
	('KW/BTU', N'Kilowatt/BTU', N'Kilowatt/BTU', 'Power/Energy', NULL),
	('MB/D', N'M Barrels/Day', N'Million Barrels/Day', 'LiqVol/Time', NULL),
	('KB/D', N'K Barrels/Day', N'Thousand Barrels/Day', 'LiqVol/Time', NULL),

	('N/A', N'Not Applicable', N'Not Applicable', 'IDX', NULL),
	('UNS', N'Unspecified', N'Unspecified', 'IDX', NULL),

	------------------------------------------------------------------------------
	--	Platts UOM Additions (Not in Platts UOM List)

	('DAG', N'Unknown (DAG)', N'Unknown (DAG)', 'IDX', NULL),
	('DMU', N'Unknown (DMU)', N'Unknown (DMU)', 'IDX', NULL),
	('LTR', N'Unknown (LTR)', N'Unknown (LTR)', 'IDX', NULL),
	('NT',  N'Unknown (NT)',  N'Unknown (NT)',  'IDX', NULL),
	('ROC', N'Unknown (ROC)', N'Unknown (ROC)', 'IDX', NULL),
	('WMT', N'Unknown (WMT)', N'Unknown (WMT)', 'IDX', NULL),
	('WSC', N'Unknown (WSC)', N'Unknown (WSC)', 'IDX', NULL)


	)[t]([UM_Tag], [UM_Name], [UM_Detail], [UM_Parent], [UM_Factor]);

INSERT INTO [dim].[UM_Uom_LookUp]([UM_Tag], [UM_Name], [UM_Detail], [UM_Factor])
SELECT [u].[UM_Tag], [u].[UM_Name], [u].[UM_Detail], [u].[UM_Factor]
FROM
	@Uom					[u]
LEFT OUTER JOIN
	[dim].[UM_Uom_LookUp]	[x]
		ON	[x].[UM_Tag]		= [u].[UM_Tag]
WHERE
	[x].[UM_UomId]	IS NULL;

INSERT INTO [dim].[UM_Uom_Parent]([UM_MethodologyId], [UM_UomId], [UM_ParentId], [UM_Operator], [UM_SortKey], [UM_Hierarchy])
SELECT 0, [c].[UM_UomId], [p].[UM_UomId], '+', 0, '/'
FROM
	@Uom							[t]
INNER JOIN
	[dim].[UM_Uom_LookUp]			[c]
		ON	[c].[UM_Tag]			= [t].[UM_Tag]
INNER JOIN
	[dim].[UM_Uom_LookUp]			[p]
		ON	[p].[UM_Tag]			= [t].[UM_Parent]
LEFT OUTER JOIN
	[dim].[UM_Uom_Parent]			[x]
		ON	[x].[UM_MethodologyId]	= 0
		AND	[x].[UM_UomId]			= [c].[UM_UomId]
		AND	[x].[UM_ParentId]		= [p].[UM_UomId]
WHERE
	[x].[UM_UomId]	IS NULL;

EXECUTE [dim].[Update_Parent] 'dim', 'UM_Uom_Parent', 'UM', 'UomId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;
