﻿SET NOCOUNT ON;

PRINT 'bulk.insert.Argus.Seed...';

DECLARE @PathSeedArgus	VARCHAR(48) = 'C:\FtpPricing\Seed\ArgusCsv\Data\';

IF OBJECT_ID('tempdb..#ArgusFile') IS NOT NULL DROP TABLE #ArgusFile;
IF OBJECT_ID('tempdb..#ArgusSeed') IS NOT NULL DROP TABLE #ArgusSeed;

CREATE TABLE #ArgusFile
(
	[PA_FilePath]				VARCHAR(260)		NOT	NULL,	CONSTRAINT [CL__PA_PricingArgusCsv_Posit_PA_FilePath]			CHECK([PA_FilePath] <> ''),
	PRIMARY KEY CLUSTERED([PA_FilePath] ASC)
)

CREATE TABLE #ArgusSeed
(
	[PA_Code]					CHAR(9)				NOT	NULL,	CHECK([PA_Code] <> ''),

	[PA_TimeStampId]			TINYINT				NOT	NULL,
	[PA_PriceTypeId]			TINYINT				NOT	NULL,
	[PA_Date]					DATE				NOT	NULL,
	[PA_Value]					DECIMAL(14, 4)		NOT	NULL,
	[PA_ContForwardPeriod]		SMALLINT			NOT	NULL,
	[PA_DiffBaseRoll]			SMALLINT			NOT	NULL,
	[PA_Year]					SMALLINT			NOT	NULL,
	[PA_ContFwd]				TINYINT				NOT	NULL,
	[PA_RecordStatus]			CHAR(1)				NOT	NULL,	CHECK([PA_RecordStatus] <> ''),

	PRIMARY KEY CLUSTERED
	(
		[PA_Code]			ASC,
		[PA_Date]			ASC,
		[PA_PriceTypeId]	ASC,
		[PA_RecordStatus]	ASC
	)
);

DECLARE @ArgusFileNames TABLE
(
	[Name]	VARCHAR(128) NOT NULL
);

INSERT INTO @ArgusFileNames
(
	[Name]
)
SELECT
	@PathSeedArgus + [t].[Name]
FROM (VALUES
	('20151113dagm.csv'),
	('20151120dagm.csv'),
	('20151127dagm.csv'),
	('20151204dagm.csv'),
	('20151211dagm.csv'),
	('20151218dagm.csv')
	) [t]([Name]);

DECLARE @NameSeedArgus	VARCHAR(128);
DECLARE @SqlSeedArgus	VARCHAR(512);

DECLARE Cur CURSOR FAST_FORWARD FOR
SELECT
	[f].[Name]
FROM
	@ArgusFileNames	[f]

OPEN Cur;

FETCH NEXT FROM Cur
INTO @NameSeedArgus;

WHILE @@FETCH_STATUS = 0
BEGIN

	DELETE FROM #ArgusFile;
	DELETE FROM #ArgusSeed;

	SET @SqlSeedArgus = 'INSERT INTO #ArgusFile([PA_FilePath]) VALUES (''' + @NameSeedArgus + ''');';
	EXECUTE(@SqlSeedArgus);

	SET @SqlSeedArgus = 'BULK INSERT #ArgusSeed FROM ''' + @NameSeedArgus + ''' WITH(FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'', FIRSTROW = 2);'
	EXECUTE(@SqlSeedArgus);

	INSERT INTO [stage].[PA_PricingArgusCsv_Posit]
	(
		[PA_FilePath],
		[PA_Code],
		[PA_TimeStampId],
		[PA_PriceTypeId],
		[PA_Date],
		[PA_Value],
		[PA_ContForwardPeriod],
		[PA_DiffBaseRoll],
		[PA_Year],
		[PA_ContFwd],
		[PA_RecordStatus],
		[PA_ChangedAt]
	)
	SELECT
		[f].[PA_FilePath],
		[s].[PA_Code],
		[s].[PA_TimeStampId],
		[s].[PA_PriceTypeId],
		[s].[PA_Date],
		[s].[PA_Value],
		[s].[PA_ContForwardPeriod],
		[s].[PA_DiffBaseRoll],
		[s].[PA_Year],
		[s].[PA_ContFwd],
		[s].[PA_RecordStatus],
		CONVERT(DATE, [s].[PA_Date], 120)
		--SYSDATETIMEOFFSET()
	FROM
		#ArgusFile	[f]
	CROSS JOIN
		#ArgusSeed	[s];

	FETCH NEXT FROM Cur
	INTO @NameSeedArgus;

END;

CLOSE Cur;
DEALLOCATE Cur;

IF OBJECT_ID('tempdb..#ArgusFile') IS NOT NULL DROP TABLE #ArgusFile;
IF OBJECT_ID('tempdb..#ArgusSeed') IS NOT NULL DROP TABLE #ArgusSeed;