﻿CREATE TABLE [fact].[PR_TRD_PricingCommodity_Trade_Posit]
(
	[PR_TRD_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[PR_TRD_PR_ID]				INT					NOT	NULL	CONSTRAINT [FK__PR_TRD_PricingCommodity_Trade_Posit_PR_ID]			REFERENCES [fact].[PR_PricingCommodity]([PR_ID]),

	[PR_TRD_Index]				FLOAT					NULL,	--	AP
	[PR_TRD_Mean]				FLOAT					NULL,	--	AP
	[PR_TRD_Ask]				FLOAT					NULL,	--	AP
	[PR_TRD_Bid]				FLOAT					NULL,	--	AP

	[PR_TRD_PrevInterest]		FLOAT					NULL,	--	 P
	[PR_TRD_Rate]				FLOAT					NULL,	--	A
	[PR_TRD_Margin]				FLOAT					NULL,	--	A

	[PR_TRD_DiffLow]			FLOAT					NULL,	--	A
	[PR_TRD_DiffHigh]			FLOAT					NULL,	--	A
	[PR_TRD_DiffIndex]			FLOAT					NULL,	--	A
	[PR_TRD_DiffMidpoint]		FLOAT					NULL,	--	A

	[PR_TRD_Midpoint]			FLOAT					NULL,	--	A
	[PR_TRD_Netback]			FLOAT					NULL,	--	A
	[PR_TRD_NetbackMargin]		FLOAT					NULL,	--	A
	[PR_TRD_RGV]				FLOAT					NULL,	--	A

	[PR_TRD_CumulativeIndex]	FLOAT					NULL,	--	A
	[PR_TRD_CumulativeVolume]	FLOAT					NULL,	--	A
	[PR_TRD_TransportCosts]		FLOAT					NULL,	--	A

	[PR_TRD_Checksum]			AS BINARY_CHECKSUM(
									[PR_TRD_Index], [PR_TRD_Mean], [PR_TRD_Ask], [PR_TRD_Bid],
									[PR_TRD_PrevInterest], [PR_TRD_Rate], [PR_TRD_Margin], 
									[PR_TRD_DiffLow], [PR_TRD_DiffHigh], [PR_TRD_DiffIndex], [PR_TRD_DiffMidpoint],
									[PR_TRD_Midpoint], [PR_TRD_Netback], [PR_TRD_NetbackMargin], [PR_TRD_RGV],
									[PR_TRD_CumulativeIndex], [PR_TRD_CumulativeVolume], [PR_TRD_TransportCosts]
									)
								PERSISTED			NOT	NULL,
	[PR_TRD_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Posit_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),

	[PR_TRD_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Posit_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PR_TRD_PricingCommodity_Trade_Posit_RowGuid]		UNIQUE NONCLUSTERED([PR_TRD_RowGuid]),

	CONSTRAINT [PK__PR_TRD_PricingCommodity_Trade_Posit] PRIMARY KEY([PR_TRD_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PR_TRD_PricingCommodity_Trade_Posit] UNIQUE CLUSTERED([PR_TRD_PR_ID] ASC, [PR_TRD_ChangedAt] DESC, [PR_TRD_Checksum] ASC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE TRIGGER [fact].[PR_TRD_PricingCommodity_Trade_Posit_Delete]
ON [fact].[PR_TRD_PricingCommodity_Trade_Posit]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_TRD_PricingCommodity_Trade_Posit].', 16, 1);
		ROLLBACK;
	END;

END;
GO