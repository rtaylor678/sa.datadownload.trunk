﻿CREATE TABLE [etl].[MP_Argus_Posit]
(
	[MP_MapId]					INT					NOT NULL	IDENTITY(1, 1),

	[MP_Code]					CHAR(9)				NOT	NULL,	CONSTRAINT [CL__MP_Argus_Posit_MP_Code]					CHECK([MP_Code] <> ''),

	[MP_FrequencyId]			INT					NOT	NULL	CONSTRAINT [FK__MP_Argus_Posit_MP_FrequencyId]			REFERENCES [dim].[FQ_Frequency_LookUp]([FQ_FrequencyId]),
	[MP_UomId]					INT					NOT	NULL	CONSTRAINT [FK__MP_Argus_Posit_MP_UomId]				REFERENCES [dim].[UM_Uom_LookUp]([UM_UomId]),
	[MP_UomMultiplier]			FLOAT				NOT	NULL,
	[MP_CurrencyId]				INT					NOT	NULL	CONSTRAINT [FK__MP_Argus_Posit_MP_CurrencyId]			REFERENCES [dim].[CU_Currency_LookUp]([CU_CurrencyId]),
	[MP_CurrencyMultiplier]		FLOAT				NOT	NULL,

	[MP_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__MP_Argus_Posit_MP_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__MP_Argus_Posit_MP_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[MP_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__MP_Argus_Posit_MP_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[MP_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__MP_Argus_Posit_MP_RowGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__MP_Argus_Posit_MP_RowGuid]				UNIQUE NONCLUSTERED([MP_RowGuid]),

	CONSTRAINT [PK__MP_Argus_Posit]		PRIMARY KEY([MP_MapId]),
	CONSTRAINT [UK__MP_Argus_Posit]		UNIQUE CLUSTERED([MP_Code] ASC),
);